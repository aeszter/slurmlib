with POSIX.Process_Environment;
with Slurm.Spread_Sheets;
with Slurm.Taint; use Slurm.Taint;
with Ada.Characters.Latin_1;

-- @summary This package calls external binaries and provides access
-- to their output
-- @description Calling binaries from a CGI is a security risk. To
-- mitigate this, the command and its arguments use special,
-- incompatible string types provided by the package Slurm.Taint.
package Slurm.Parser is
   package Env renames POSIX.Process_Environment;

   Parser_Error : exception;
   -- can be raised if an error is encountered when dealing with
   -- external commands

   procedure Setup (Command   : Trusted_Command_Name;
                    Arguments : Trusted_String_List;
                    Output    : out Spread_Sheets.Spread_Sheet;
                    Exit_Status : out Natural;
                    Field_Separator : Character := Ada.Characters.Latin_1.HT;
                    Standard_Separator : Boolean := True);
   -- call an external command, passing its output as a Spread_Sheet.
   -- New rows are started with a line feed; new cells are either
   -- started with the character given in Field_Separator, or with
   -- tabs or spaces (Standard Separator).
   -- @param Command the name of the command to call
   -- @param Arguments any arguments to pass
   -- @param Output the data output by the command
   -- @param Exit_Status the exit status of the command: 0 usually
   -- means success, other values are implementation-defined
   -- @param Field_Separator if Standard_Separator is False, this
   -- character separates cells
   -- @param Standard_Separator if True, a new cell is started when
   -- either a tab or onne or more spaces is encountered; if it's
   -- False, a new cell is started after each occurrence of
   -- Field_Separator.
   -- @exception Parser_Error if the external command couldn't be
   -- spawned, it it terminated with a signal, of if an exception was
   -- raised by the Spread_Sheet

private
   Slurm_Path : constant String := POSIX.To_String (Env.Environment_Value_Of ("SLURM_PATH"));

end Slurm.Parser;
