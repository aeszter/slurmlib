with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Ordered_Maps;
with Interfaces;
with Slurm.Loggers;
with Slurm.Utils;

package Slurm.Partitions is

   type Partition is new Loggers.Logger with private;
   type List is private;
   type Cursor is private;
   function Element (Position : Cursor) return Partition;
   function Has_Element (Position : Cursor) return Boolean;
   function First (Collection : List) return Cursor;
   procedure Next (Position : in out Cursor);
   procedure Append (Collection : in out List; Item : Partition);
   procedure Iterate (Collection : List;
                      Process    : not null access procedure (Position : Cursor));
   function Load_Partitions return List;

   type states is (PARTITION_INACTIVE,
                   PARTITION_DOWN,
                   PARTITION_DRAIN,
                   PARTITION_UP);
   type Preempt_Mode is (preempt_off, preempt_suspend,
                         preempt_requeue,
                         preempt_cancel,
                         preempt_qos,
                         preempt_gang);

   function Get_Allocation_Nodes (P : Partition) return String;
   function Get_Accounts (P : Partition) return String;
   function Get_Deny_Accounts (P : Partition) return String;
   function Get_Groups (P : Partition) return String;
   function Get_QOS (P : Partition) return String;
   function Get_Deny_QOS (P : Partition) return String;
   function Get_Alternate (P : Partition) return String;
   function Get_Billing_Weights (P : Partition) return String;
   function Get_Default_Memory_Per_CPU (P : Partition) return Slurm.Utils.Gigs;
   function Get_Default_Time (P : Partition) return Duration;
   function Get_Grace_Time (P : Partition) return Duration;
   function Get_Max_CPUs_Per_Node (P : Partition) return Integer;
   function Get_Max_Memory_Per_CPU (P : Partition) return Slurm.Utils.Gigs;
   function Get_Max_Nodes (P : Partition) return Integer;
   function Get_Max_Share (P : Partition) return Integer;
   function Get_Max_Time (P : Partition) return Duration;
   function Get_Min_Nodes (P : Partition) return Integer;
   function Get_Name (P : Partition) return String;
   function Get_Nodes (P : Partition) return String;
   function Get_Node_Number (P : Partition) return Natural;
   function Get_CPUs_Per_GPU (P : Partition) return Natural;
   function Get_Preempt_Mode (P : Partition) return Preempt_Mode;
   function Get_Preempt_Mode (P : Partition) return String;
   function Get_Priority_Job_Factor (P : Partition) return Integer;
   function Get_Priority_Tier (P : Partition) return Integer;
   function Get_QOS_Name (P : Partition) return String;
   function Get_State_Up (P : Partition) return states;
   function Get_Total_CPUs (P : Partition) return Natural;
   function Get_Total_Nodes (P : Partition) return Natural;
   function Get_TRES (P : Partition) return String;

   function Is_Default (P : Partition) return Boolean;
   function Is_Hidden (P : Partition) return Boolean;
   function Is_No_Root (P : Partition) return Boolean;
   function Is_Root_Only (P : Partition) return Boolean;
   function Requires_Reservation (P : Partition) return Boolean;
   function Is_Least_Loaded (P : Partition) return Boolean;
   function Is_Exclusive_User (P : Partition) return Boolean;
   function Is_Idle_Power_Down (P : Partition) return Boolean;
   function Has_CR_CPU (P : Partition) return Boolean;
   function Has_CR_Socket (P : Partition) return Boolean;
   function Has_CR_Core (P : Partition) return Boolean;
   function Has_CR_Board (P : Partition) return Boolean;
   function Has_CR_Memory (P : Partition) return Boolean;

   function Get_Partition (Collection : List; Name : String) return Partition;
private
   type Partition is new Loggers.Logger with record
      Allocation_Nodes          : Unbounded_String;
      Accounts                  : Unbounded_String;
      CPUs_per_GPU              : Natural;
      Deny_Accounts             : Unbounded_String;
      Groups                    : Unbounded_String;
      QOS                       : Unbounded_String;
      CR_CPU, CR_Core, CR_Socket, CR_Board,
      CR_Memory                 : Boolean;
      Deny_QOS                  : Unbounded_String;
      Alternate                 : Unbounded_String;
      Billing_Weights           : Unbounded_String;
      Default_Memory_Per_CPU    : Slurm.Utils.Gigs;
      Default_Time              : Duration;
      Default, Hidden, No_Root, Root_Only,
      Reservation_Required, Least_Loaded,
      Exclusive_User, Power_Down_On_Idle : Boolean;
      Grace_Time                : Duration;
      Max_CPUs_Per_Node         : Integer;
      Max_Memory_Per_CPU        : Slurm.Utils.Gigs;
      Max_Nodes                 : Integer;
      Max_Share                 : Integer;
      Max_Time                  : Duration;
      Min_Nodes                 : Integer;
      Name                      : Unbounded_String;
      Nodes                     : Unbounded_String;
      Preempt                   : Preempt_Mode;
      Priority_Job_Factor       : Integer;
      Priority_Tier             : Integer;
      QOS_Name                  : Unbounded_String;
      State_Up                  : states;
      Total_CPUs                : Natural;
      Total_Nodes               : Natural;
      TRES                      : Unbounded_String;
   end record;

   package Lists is new Ada.Containers.Ordered_Maps (Element_Type => Partition,
                                                     Key_Type =>
                                                     Unbounded_String);
   type Cursor is new Lists.Cursor;
   type List is record
      Container : Lists.Map;
   end record;

end Slurm.Partitions;
