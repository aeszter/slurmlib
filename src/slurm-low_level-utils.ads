with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Calendar;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with POSIX.C;
with Ada.Containers;
with Ada.Containers.Ordered_Sets;
with Ada.Containers.Doubly_Linked_Lists;
with Slurm.Low_Level.bits_stdint_uintn_h; use Slurm.Low_Level.bits_stdint_uintn_h;
with Slurm.Low_Level.bits_types_time_t_h; use Slurm.Low_Level.bits_types_time_t_h;
with Slurm.Utils;

package Slurm.Low_Level.Utils is

   type Fixed is delta 0.0001 digits 5;
   --  a general fixed type, especially useful for SGE resources

   function To_String (Source : Interfaces.C.Strings.chars_ptr) return String;
   function Convert_String (Source : chars_ptr) return Unbounded_String;
   function Convert_Time (Source : time_t) return Ada.Calendar.Time;
   function Convert_Minutes (Source : Interfaces.C.unsigned) return Duration;
   function Convert_Seconds (Source : Interfaces.C.unsigned) return Duration;
   function Convert_Integer (Source : uint32_t) return Integer;

   function MiB_To_Gigs (Source : uint64_t) return Slurm.Utils.Gigs;
   function MiB_To_Gigs (Source : uint32_t) return Slurm.Utils.Gigs;

end Slurm.Low_Level.Utils;
