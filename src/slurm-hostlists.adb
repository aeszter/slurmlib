with Interfaces.C.Strings; use Interfaces.C.Strings;

with Slurm.Low_Level.slurm_slurm_h;
with Slurm.Errors;

package body Slurm.Hostlists is
   overriding function "<" (Left, Right : Node_Name) return Boolean is
   begin
      return Ada.Strings.Unbounded."<" (Unbounded_String (Left), Unbounded_String (Right));
   end "<";

   overriding function "=" (Left, Right : Node_Name) return Boolean is
   begin
      return Ada.Strings.Unbounded."=" (Unbounded_String (Left), Unbounded_String (Right));
   end "=";

   function To_Hostlist (Source : String) return Hostlist is
      use Slurm.Low_Level.slurm_slurm_h;
      Result : Hostlist := Name_Sets.Empty_Set;
      Next_Host : chars_ptr;
      hl : hostlistPtr := slurm_hostlist_create (New_String (Source));
   begin
      loop
         Next_Host := slurm_hostlist_shift (hl);
         exit when Next_Host = Null_Ptr;
         Result.Include (To_Node_Name (Value (Next_Host)));
         Free (Next_Host);
      end loop;
      slurm_hostlist_destroy (hl);
      return Result;
   end To_Hostlist;

   function To_Node_Name (Source : String) return Node_Name is
   begin
      return Node_Name (Ada.Strings.Unbounded.To_Unbounded_String (Source));
   end To_Node_Name;

   function To_Ranged_String (Source : String) return String is
      use Slurm.Low_Level.slurm_slurm_h;

      h1 : hostlistPtr := slurm_hostlist_create (New_String (Source));
   begin
      return Value (slurm_hostlist_ranged_string_xmalloc (h1));
   end To_Ranged_String;

   overriding function To_String (Source : Node_Name) return String is
   begin
      return To_String (Unbounded_String (Source));
   end To_String;

   function To_String (Source : Hostlist) return String is
      use Slurm.Low_Level.slurm_slurm_h;
      use Interfaces.C;
      use Name_Sets;

      procedure Append (Position : Cursor);

      h1 : hostlistPtr := slurm_hostlist_create (Null_Ptr);

      procedure Append (Position : Cursor) is
         use Slurm.Errors;
         H : String := To_String (Element (Position));
      begin
         if slurm_hostlist_push_host (h1, New_String (H)) = 0 then
            raise Slurm_Error with "unable to push " & H;
         end if;
      end Append;

   begin
      Source.Iterate (Append'Access);
      return Value (slurm_hostlist_ranged_string_xmalloc (h1));
   end To_String;

end Slurm.Hostlists;
