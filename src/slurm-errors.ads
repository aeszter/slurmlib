with POSIX.C; use POSIX.C;

-- @summary A collection of types and routines to deal with errors raised by
-- the Slurm API.
package Slurm.Errors is

   Slurm_Error : exception;
   -- this exception can be raised by the library to notify the
   -- application of an error inside the API

   type Error is new int;
   -- the API reports errors with an it error number; use a separate
   -- type in Ada

   Protocol_Version_Error : constant Error := 1005;
   -- an incompatibility between Slurm versions
   Socket_Timeout : constant Error := 5004;
   -- a timeout occurred innn communnication between Slurm components
   User_ID_Missing : constant Error := 2010;
   -- a UID couldn't be resolved
   function Get_Last_Error return Error;
   -- @return the error code set by the last Slurm API function executed.
   function Get_Error (errno : Error) return String;
  -- get an explanation for an error number
  -- @param errno the error number to look up
  -- @Return a string describing a specific Slurm error code.
end Slurm.Errors;
