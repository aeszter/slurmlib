with Ada.Containers.Doubly_Linked_Lists;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

-- @summary This package deals with strings that are passed to the
-- command line.
-- @description While much information can be gathered from Slurm
-- directly via the API, in some cases it is necessary to call an
-- external binary. Such binaries typically take arguments pertaining
-- e.g. to individual jobs, so they're often user-supplied. This poses
-- a security risk, especially if the program using this library is
-- operated remotely (e.g. through a web server).
-- Therefore, this package supplies private string types that can be
-- used by packages that need to call external binaries. Data supplied
-- by users will be stored in standard Strings, Unbounded_Strings an
-- possibly other data types. Thus, a strict boundary between
-- user-supplied data and data that is passed to the command line is
-- erected, and this package is the only place that allows data to
-- cross this boundary (by transforming Strings to Trusted_Strings).
-- However, care must be taken to use conversion routines responsibly.
-- See documentation of these routines for details.
package Slurm.Taint is

   --  data of type Trusted_String can be passed as parameters to external programs
   type Trusted_String (<>) is private;

   function "&" (Left, Right : Trusted_String) return Trusted_String;
   -- Concatenate two Trusted_Strings
   -- @param Left the first of two strings to concatenate
   -- @param Right the second of two strings to concatenate
   -- @return the concatenation of Left and Right; Value (L & R) =
   -- Value (L) & Value (R)

   --  data of type Trusted_Command_Name can be used to call external programs across
   --  privilege borders
   type Trusted_Command_Name (<>) is private;

   --  Extract the string value from a trusted string
   function Value (S : Trusted_String) return String;
   -- @param S the trusted string to convert
   -- @return the contents of S

   --  Extract the string value from a trusted string
   function Value (S : Trusted_Command_Name) return String;
   -- @param S the trusted string to convert
   -- @return the contents of S

   --  convert untrusted user data to a trusted string by removing/replacing
   --  any offending characters
   function Sanitise (S : String) return Trusted_String;
   -- @param S the string to convert
   -- @return a Trusted_String equivalent to S, with any characters
   -- that are not alphanumeric and do not belong to a small set of
   -- harmless punctuation characters replaced

   --  convert untrusted user data to a trusted string by removing/replacing
   --  any offending characters; stricter check assumes S represents a number
   function Sanitise_Number (S : String) return Trusted_String;
   -- @param S the string to convert
   -- @return a Trusted_String equivalent to S, with any characters
   -- that are not numeric replaced

   --  convert an implicitly trusted String to a Trusted_String;
   --  Never!! pass untrusted user data to this function.
   --  It is meant for program-internal data only.
   function Implicit_Trust (S : String) return Trusted_String;
   -- @param S the string to convert; should be a literal
   -- @return a Trusted_String equivalent to S; no characters are
   -- changed, so this should only be called for string literals!

   --  convert an implicitly trusted String to a Trusted_Command;
   --  Never!! pass untrusted user data to this function.
   --  It is meant for program-internal data only.
   function Trust_As_Command (S : String) return Trusted_Command_Name;
   -- @param S the string to convert; should be a literal
   -- @return a Trusted_Command_Name equivalent to S; no characters are
   -- changed, so this should only be called for string literals!

   Cmd_Sprio : constant Trusted_Command_Name;
   -- the Slurm command sprio
   Cmd_Sshare : constant Trusted_Command_Name;
   -- the Slurm command sshare

   type Trusted_String_List is tagged private;
   -- a list of Trusted_Strings

   type Cursor is private;
   -- a cursor pointing into a Trusted_String_List

   procedure Append (Source : in out Trusted_String_List;
                     New_Item : Trusted_String);
   -- append a Trusted_String to a list
   -- @param Source the List to append to
   -- @param New_Item the Trusted_String to append
   function First (Collection : Trusted_String_List) return Cursor;
   -- get the first entry in a list
   -- @param Collection the list to look at
   -- @return a Cursor pointing to the first entry of the list
   function Has_Element (Position : Cursor) return Boolean;
   -- @param Position the cursor to look at
   -- @return whether Position points at a Trusted_String
   procedure Next (Position : in out Cursor);
   -- get the next entry in a list
   -- @param Position upon return, Position points one element further
   -- than what Next was called with
   function Element (Position : Cursor) return Trusted_String;
   -- @param Position the cursor to look at
   -- @return the element Position points at
   -- @exception raise Constraint_Error if not Has_Element(Position)
   function Value (Collection : Trusted_String_List) return String;
   -- Convert a list to a string
   -- @param Collection the list to concatenate
   -- @return the concatenation of all the elements of Collection

private
   type Trusted_String is new String;
   type Trusted_Command_Name is new String;

   Cmd_Sprio : constant Trusted_Command_Name := "sprio";
   Cmd_Sshare : constant Trusted_Command_Name := "sshare";

   package Lists is new ada.Containers.Doubly_Linked_Lists (Unbounded_String);
   type Trusted_String_List is new Lists.List with null record;
   type Cursor is new Lists.Cursor;
end Slurm.Taint;
