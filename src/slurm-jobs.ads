with Ada.Calendar;
with Ada.Containers;
with Ada.Containers.Ordered_Maps;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Slurm.Loggers;
with Slurm.Utils; use Slurm.Utils;
with Ada.Containers.Doubly_Linked_Lists;
with Slurm.Hostlists; use Slurm.Hostlists;
with Interfaces;

-- @summary This package deals with jobs. It provides routines to
-- load, select, and summarize jobs; many attributes of jobs are
-- supplied. There is also a host of constants that pertain to jobs.
package Slurm.Jobs is
   type states is (
        JOB_PENDING, -- waiting to be executed
        JOB_RUNNING, -- being executed
        JOB_SUSPENDED, -- held after it has been started
        JOB_COMPLETE, -- finished excution
        JOB_CANCELLED, -- revoked
        JOB_FAILED,    -- ended with an error
        JOB_TIMEOUT,   -- exceeded its allotted time
        JOB_NODE_FAIL, -- stopped because of a node failure
        JOB_PREEMPTED, -- superceded by a higher-priority job
        JOB_BOOT_FAIL, -- a required node failed to boot
        JOB_DEADLINE,  -- terminated on deadline
        JOB_OOM -- out of memory
        );
   -- basic states a job may be in
   type State_Count is array (states) of Natural;
   -- how many jobs are there in each of the states

   type state_reasons is (
-- Reasons for job to be pending
        WAIT_NO_REASON, -- not set or job not pending */
        WAIT_PRIORITY,          -- higher priority jobs exist */
        WAIT_DEPENDENCY,        -- dependent job has not completed */
        WAIT_RESOURCES,         -- required resources not available */
        WAIT_PART_NODE_LIMIT,   -- request exceeds partition node limit */
        WAIT_PART_TIME_LIMIT,   -- request exceeds partition time limit */
        WAIT_PART_DOWN,         -- requested partition is down */
        WAIT_PART_INACTIVE,     -- requested partition is inactive */
        WAIT_HELD,              -- job is held by administrator */
        WAIT_TIME,              -- job waiting for specific begin time */
        WAIT_LICENSES,          -- job is waiting for licenses */
        WAIT_ASSOC_JOB_LIMIT,   -- user/bank job limit reached */
         WAIT_ASSOC_RESOURCE_LIMIT, -- user/bank resource limit reached */
        WAIT_ASSOC_TIME_LIMIT,  -- user/bank time limit reached */
        WAIT_RESERVATION,       -- reservation not available */
        WAIT_NODE_NOT_AVAIL,    -- required node is DOWN or DRAINED */
        WAIT_HELD_USER,         -- job is held by user */
        WAIT_FRONT_END,         -- Front end nodes are DOWN */
        FAIL_DEFER,             -- individual submit time sched deferred
        FAIL_DOWN_PARTITION,    -- partition for job is DOWN */
        FAIL_DOWN_NODE,         -- some node in the allocation failed */
        FAIL_BAD_CONSTRAINTS,   -- constraints can not be satisfied */
        FAIL_SYSTEM,            -- slurm system failure */
        FAIL_LAUNCH,            -- unable to launch job */
        FAIL_EXIT_CODE,         -- exit code was non-zero */
        FAIL_TIMEOUT,           -- reached end of time limit */
        FAIL_INACTIVE_LIMIT,    -- reached slurm InactiveLimit */
        FAIL_ACCOUNT,           -- invalid account */
        FAIL_QOS,               -- invalid QOS */
        WAIT_QOS_THRES,         -- required QOS threshold has been breached */
        WAIT_QOS_JOB_LIMIT,     -- QOS job limit reached */
         WAIT_QOS_RESOURCE_LIMIT, -- QOS resource limit reached */
        WAIT_QOS_TIME_LIMIT,    -- QOS time limit reached */
        WAIT_BLOCK_MAX_ERR,     -- BLUEGENE Block has too many cnodes
                                 -- in error state to allow more jobs. */
        WAIT_BLOCK_D_ACTION,    -- BLUEGENE Block is being freed,
                           -- can't allow more jobs. */
        WAIT_CLEANING,          -- If a job is requeued and it is
                                 -- still cleaning up from the last run. */
        WAIT_PROLOG,            -- Prolog is running */
        WAIT_QOS,               -- QOS not allowed */
        WAIT_ACCOUNT,           -- Account not allowed */
        WAIT_DEP_INVALID,        -- Dependency condition invalid or never
                           -- satisfied

        WAIT_QOS_GRP_CPU,            -- QOS GrpTRES exceeded (CPU) */
        WAIT_QOS_GRP_CPU_MIN,        -- QOS GrpTRESMins exceeded (CPU) */
        WAIT_QOS_GRP_CPU_RUN_MIN,    -- QOS GrpTRESRunMins exceeded (CPU) */
        WAIT_QOS_GRP_JOB,            -- QOS GrpJobs exceeded */
        WAIT_QOS_GRP_MEM,            -- QOS GrpTRES exceeded (Memory) */
        WAIT_QOS_GRP_NODE,           -- QOS GrpTRES exceeded (Node) */
        WAIT_QOS_GRP_SUB_JOB,        -- QOS GrpSubmitJobs exceeded */
        WAIT_QOS_GRP_WALL,           -- QOS GrpWall exceeded */
        WAIT_QOS_MAX_CPU_PER_JOB,    -- QOS MaxTRESPerJob exceeded (CPU) */
         WAIT_QOS_MAX_CPU_MINS_PER_JOB, -- QOS MaxTRESMinsPerJob exceeded (CPU) */
        WAIT_QOS_MAX_NODE_PER_JOB,   -- QOS MaxTRESPerJob exceeded (Node) */
        WAIT_QOS_MAX_WALL_PER_JOB,   -- QOS MaxWallDurationPerJob exceeded */
        WAIT_QOS_MAX_CPU_PER_USER,   -- QOS MaxTRESPerUser exceeded (CPU) */
        WAIT_QOS_MAX_JOB_PER_USER,   -- QOS MaxJobsPerUser exceeded */
        WAIT_QOS_MAX_NODE_PER_USER,  -- QOS MaxTRESPerUser exceeded (Node) */
        WAIT_QOS_MAX_SUB_JOB,        -- QOS MaxSubmitJobsPerUser exceeded */
        WAIT_QOS_MIN_CPU,            -- QOS MinTRESPerJob not reached (CPU) */
        WAIT_ASSOC_GRP_CPU,          -- ASSOC GrpTRES exceeded (CPU) */
        WAIT_ASSOC_GRP_CPU_MIN,      -- ASSOC GrpTRESMins exceeded (CPU) */
        WAIT_ASSOC_GRP_CPU_RUN_MIN,  -- ASSOC GrpTRESRunMins exceeded (CPU) */
        WAIT_ASSOC_GRP_JOB,          -- ASSOC GrpJobs exceeded */
        WAIT_ASSOC_GRP_MEM,          -- ASSOC GrpTRES exceeded (Memory) */
        WAIT_ASSOC_GRP_NODE,         -- ASSOC GrpTRES exceeded (Node) */
        WAIT_ASSOC_GRP_SUB_JOB,      -- ASSOC GrpSubmitJobs exceeded */
        WAIT_ASSOC_GRP_WALL,         -- ASSOC GrpWall exceeded */
        WAIT_ASSOC_MAX_JOBS,         -- ASSOC MaxJobs exceeded */
        WAIT_ASSOC_MAX_CPU_PER_JOB,  -- ASSOC MaxTRESPerJob exceeded (CPU) */
         WAIT_ASSOC_MAX_CPU_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob
                           -- exceeded (CPU) */
        WAIT_ASSOC_MAX_NODE_PER_JOB, -- ASSOC MaxTRESPerJob exceeded (NODE) */
        WAIT_ASSOC_MAX_WALL_PER_JOB, -- ASSOC MaxWallDurationPerJob
                           -- exceeded */
        WAIT_ASSOC_MAX_SUB_JOB,      -- ASSOC MaxSubmitJobsPerUser exceeded */

        WAIT_MAX_REQUEUE,            -- MAX_BATCH_REQUEUE reached */
        WAIT_ARRAY_TASK_LIMIT,       -- job array running task limit */
        WAIT_BURST_BUFFER_RESOURCE,  -- Burst buffer resources */
        WAIT_BURST_BUFFER_STAGING,   -- Burst buffer file stage-in */
        FAIL_BURST_BUFFER_OP,        -- Burst buffer operation failure */
        WAIT_POWER_NOT_AVAIL,        -- not enough power available */
        WAIT_POWER_RESERVED,         -- job is waiting for available power
                           -- because of power reservations */
        WAIT_ASSOC_GRP_UNK,          -- ASSOC GrpTRES exceeded
                           -- (Unknown) */
        WAIT_ASSOC_GRP_UNK_MIN,      -- ASSOC GrpTRESMins exceeded
                           -- (Unknown) */
        WAIT_ASSOC_GRP_UNK_RUN_MIN,  -- ASSOC GrpTRESRunMins exceeded
                           -- (Unknown) */
        WAIT_ASSOC_MAX_UNK_PER_JOB,  -- ASSOC MaxTRESPerJob exceeded
                           -- (Unknown) */
        WAIT_ASSOC_MAX_UNK_PER_NODE,  -- ASSOC MaxTRESPerNode exceeded
                                       -- (Unknown) */
         WAIT_ASSOC_MAX_UNK_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob
                           -- exceeded (Unknown) */
        WAIT_ASSOC_MAX_CPU_PER_NODE,  -- ASSOC MaxTRESPerNode exceeded (CPU) */
        WAIT_ASSOC_GRP_MEM_MIN,      -- ASSOC GrpTRESMins exceeded
                           -- (Memory) */
        WAIT_ASSOC_GRP_MEM_RUN_MIN,  -- ASSOC GrpTRESRunMins exceeded
                           -- (Memory) */
        WAIT_ASSOC_MAX_MEM_PER_JOB,  -- ASSOC MaxTRESPerJob exceeded (Memory) */
        WAIT_ASSOC_MAX_MEM_PER_NODE,  -- ASSOC MaxTRESPerNode exceeded (CPU) */
         WAIT_ASSOC_MAX_MEM_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob
                           -- exceeded (Memory) */
        WAIT_ASSOC_GRP_NODE_MIN,     -- ASSOC GrpTRESMins exceeded (Node) */
        WAIT_ASSOC_GRP_NODE_RUN_MIN, -- ASSOC GrpTRESRunMins exceeded (Node) */
         WAIT_ASSOC_MAX_NODE_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob
                                          -- exceeded (Node) */
        WAIT_ASSOC_GRP_ENERGY,           -- ASSOC GrpTRES exceeded
                                          -- (Energy) */
        WAIT_ASSOC_GRP_ENERGY_MIN,       -- ASSOC GrpTRESMins exceeded
                                          -- (Energy) */
        WAIT_ASSOC_GRP_ENERGY_RUN_MIN,   -- ASSOC GrpTRESRunMins exceeded
                                          -- (Energy) */
        WAIT_ASSOC_MAX_ENERGY_PER_JOB,   -- ASSOC MaxTRESPerJob exceeded
                                          -- (Energy) */
        WAIT_ASSOC_MAX_ENERGY_PER_NODE,  -- ASSOC MaxTRESPerNode
                                          -- exceeded (Energy) */
         WAIT_ASSOC_MAX_ENERGY_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob
                           -- exceeded (Energy) */
        WAIT_ASSOC_GRP_GRES,          -- ASSOC GrpTRES exceeded (GRES) */
        WAIT_ASSOC_GRP_GRES_MIN,      -- ASSOC GrpTRESMins exceeded (GRES) */
        WAIT_ASSOC_GRP_GRES_RUN_MIN,  -- ASSOC GrpTRESRunMins exceeded (GRES) */
        WAIT_ASSOC_MAX_GRES_PER_JOB,  -- ASSOC MaxTRESPerJob exceeded (GRES) */
        WAIT_ASSOC_MAX_GRES_PER_NODE, -- ASSOC MaxTRESPerNode exceeded (GRES) */
         WAIT_ASSOC_MAX_GRES_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob
                                          -- exceeded (GRES) */
        WAIT_ASSOC_GRP_LIC,          -- ASSOC GrpTRES exceeded
                           -- (license) */
        WAIT_ASSOC_GRP_LIC_MIN,      -- ASSOC GrpTRESMins exceeded
                           -- (license) */
        WAIT_ASSOC_GRP_LIC_RUN_MIN,  -- ASSOC GrpTRESRunMins exceeded
                           -- (license) */
        WAIT_ASSOC_MAX_LIC_PER_JOB,  -- ASSOC MaxTRESPerJob exceeded
                           -- (license) */
         WAIT_ASSOC_MAX_LIC_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob exceeded
                           -- (license) */
        WAIT_ASSOC_GRP_BB,          -- ASSOC GrpTRES exceeded
                           -- (burst buffer) */
        WAIT_ASSOC_GRP_BB_MIN,      -- ASSOC GrpTRESMins exceeded
                           -- (burst buffer) */
        WAIT_ASSOC_GRP_BB_RUN_MIN,  -- ASSOC GrpTRESRunMins exceeded
                           -- (burst buffer) */
        WAIT_ASSOC_MAX_BB_PER_JOB,  -- ASSOC MaxTRESPerJob exceeded
                           -- (burst buffer) */
        WAIT_ASSOC_MAX_BB_PER_NODE, -- ASSOC MaxTRESPerNode exceeded
                           -- (burst buffer) */
        WAIT_ASSOC_MAX_BB_MINS_PER_JOB, -- ASSOC MaxTRESMinsPerJob exceeded
                           -- (burst buffer) */
        WAIT_QOS_GRP_UNK,           -- QOS GrpTRES exceeded (Unknown) */
        WAIT_QOS_GRP_UNK_MIN,       -- QOS GrpTRESMins exceeded (Unknown) */
        WAIT_QOS_GRP_UNK_RUN_MIN,   -- QOS GrpTRESRunMins exceeded (Unknown) */
        WAIT_QOS_MAX_UNK_PER_JOB,   -- QOS MaxTRESPerJob exceeded (Unknown) */
        WAIT_QOS_MAX_UNK_PER_NODE,  -- QOS MaxTRESPerNode exceeded (Unknown) */
        WAIT_QOS_MAX_UNK_PER_USER,  -- QOS MaxTRESPerUser exceeded (Unknown) */
         WAIT_QOS_MAX_UNK_MINS_PER_JOB, -- QOS MaxTRESMinsPerJob
                                       -- exceeded (Unknown) */
        WAIT_QOS_MIN_UNK,           -- QOS MinTRESPerJob exceeded (Unknown) */
        WAIT_QOS_MAX_CPU_PER_NODE,  -- QOS MaxTRESPerNode exceeded (CPU) */
        WAIT_QOS_GRP_MEM_MIN,       -- QOS GrpTRESMins exceeded
                           -- (Memory) */
        WAIT_QOS_GRP_MEM_RUN_MIN,   -- QOS GrpTRESRunMins exceeded
                           -- (Memory) */
         WAIT_QOS_MAX_MEM_MINS_PER_JOB, -- QOS MaxTRESMinsPerJob
                                       -- exceeded (Memory) */
        WAIT_QOS_MAX_MEM_PER_JOB,   -- QOS MaxTRESPerJob exceeded (CPU) */
        WAIT_QOS_MAX_MEM_PER_NODE,  -- QOS MaxTRESPerNode exceeded (MEM) */
        WAIT_QOS_MAX_MEM_PER_USER,  -- QOS MaxTRESPerUser exceeded (CPU) */
        WAIT_QOS_MIN_MEM,           -- QOS MinTRESPerJob not reached (Memory) */
        WAIT_QOS_GRP_ENERGY,        -- QOS GrpTRES exceeded (Energy) */
        WAIT_QOS_GRP_ENERGY_MIN,    -- QOS GrpTRESMins exceeded (Energy) */
        WAIT_QOS_GRP_ENERGY_RUN_MIN, -- QOS GrpTRESRunMins exceeded (Energy) */
        WAIT_QOS_MAX_ENERGY_PER_JOB, -- QOS MaxTRESPerJob exceeded (Energy) */
        WAIT_QOS_MAX_ENERGY_PER_NODE, -- QOS MaxTRESPerNode exceeded (Energy) */
         WAIT_QOS_MAX_ENERGY_PER_USER, -- QOS MaxTRESPerUser exceeded (Energy) */
        WAIT_QOS_MAX_ENERGY_MINS_PER_JOB, -- QOS MaxTRESMinsPerJob
                                          -- exceeded (Energy) */
        WAIT_QOS_MIN_ENERGY,        -- QOS MinTRESPerJob not reached (Energy) */
        WAIT_QOS_GRP_NODE_MIN,     -- QOS GrpTRESMins exceeded (Node) */
        WAIT_QOS_GRP_NODE_RUN_MIN, -- QOS GrpTRESRunMins exceeded (Node) */
        WAIT_QOS_MAX_NODE_MINS_PER_JOB,  -- QOS MaxTRESMinsPerJob
                                          -- exceeded (Node) */
        WAIT_QOS_MIN_NODE,          -- QOS MinTRESPerJob not reached (Node) */
        WAIT_QOS_GRP_GRES,          -- QOS GrpTRES exceeded (GRES) */
        WAIT_QOS_GRP_GRES_MIN,      -- QOS GrpTRESMins exceeded (GRES) */
        WAIT_QOS_GRP_GRES_RUN_MIN,  -- QOS GrpTRESRunMins exceeded (GRES) */
        WAIT_QOS_MAX_GRES_PER_JOB,  -- QOS MaxTRESPerJob exceeded (GRES) */
        WAIT_QOS_MAX_GRES_PER_NODE, -- QOS MaxTRESPerNode exceeded (GRES) */
        WAIT_QOS_MAX_GRES_PER_USER, -- QOS MaxTRESPerUser exceeded
                           -- (GRES) */
         WAIT_QOS_MAX_GRES_MINS_PER_JOB, -- QOS MaxTRESMinsPerJob
                           -- exceeded (GRES) */
        WAIT_QOS_MIN_GRES,          -- QOS MinTRESPerJob not reached (CPU) */
        WAIT_QOS_GRP_LIC,           -- QOS GrpTRES exceeded (license) */
        WAIT_QOS_GRP_LIC_MIN,       -- QOS GrpTRESMins exceeded (license) */
        WAIT_QOS_GRP_LIC_RUN_MIN,   -- QOS GrpTRESRunMins exceeded (license) */
        WAIT_QOS_MAX_LIC_PER_JOB,   -- QOS MaxTRESPerJob exceeded (license) */
        WAIT_QOS_MAX_LIC_PER_USER,  -- QOS MaxTRESPerUser exceeded (license) */
         WAIT_QOS_MAX_LIC_MINS_PER_JOB, -- QOS MaxTRESMinsPerJob exceeded
                                       -- (license) */
        WAIT_QOS_MIN_LIC,           -- QOS MinTRESPerJob not reached
                           -- (license) */
        WAIT_QOS_GRP_BB,            -- QOS GrpTRES exceeded
                           -- (burst buffer) */
        WAIT_QOS_GRP_BB_MIN,        -- QOS GrpTRESMins exceeded
                           -- (burst buffer) */
        WAIT_QOS_GRP_BB_RUN_MIN,    -- QOS GrpTRESRunMins exceeded
                           -- (burst buffer) */
        WAIT_QOS_MAX_BB_PER_JOB,   -- QOS MaxTRESPerJob exceeded
                                    -- (burst buffer) */
        WAIT_QOS_MAX_BB_PER_NODE,  -- QOS MaxTRESPerNode exceeded
                                    -- (burst buffer) */
        WAIT_QOS_MAX_BB_PER_USER,  -- QOS MaxTRESPerUser exceeded
                                    -- (burst buffer) */
         WAIT_QOS_MAX_BB_MINS_PER_JOB, -- QOS MaxTRESMinsPerJob exceeded
                           -- (burst buffer) */
        WAIT_QOS_MIN_BB,           -- QOS MinTRESPerJob not reached
                                    -- (burst buffer) */
        FAIL_DEADLINE,              -- reached deadline */
        -- QOS MaxTRESPerAccount */
        WAIT_QOS_MAX_BB_PER_ACCT,     -- exceeded burst buffer */
        WAIT_QOS_MAX_CPU_PER_ACCT,    -- exceeded CPUs */
        WAIT_QOS_MAX_ENERGY_PER_ACCT, -- exceeded Energy */
        WAIT_QOS_MAX_GRES_PER_ACCT,   -- exceeded GRES */
        WAIT_QOS_MAX_NODE_PER_ACCT,   -- exceeded Nodes */
        WAIT_QOS_MAX_LIC_PER_ACCT,    -- exceeded Licenses */
        WAIT_QOS_MAX_MEM_PER_ACCT,    -- exceeded Memory */
        WAIT_QOS_MAX_UNK_PER_ACCT,    -- exceeded Unknown */
        --*******************/
        WAIT_QOS_MAX_JOB_PER_ACCT,    -- QOS MaxJobPerAccount exceeded */
         WAIT_QOS_MAX_SUB_JOB_PER_ACCT, -- QOS MaxJobSubmitSPerAccount exceeded */
        WAIT_PART_CONFIG,             -- Generic partition configuration reason */
        WAIT_ACCOUNT_POLICY,          -- Generic accounting policy reason */

        WAIT_FED_JOB_LOCK,            -- Can't get fed job lock */
        FAIL_OOM,                     -- Exhausted memory */
        WAIT_PN_MEM_LIMIT,            -- MaxMemPer[CPU|Node] exceeded */

        -- exceeded Billing TRES limits */
        WAIT_ASSOC_GRP_BILLING,             -- GrpTRES           */
        WAIT_ASSOC_GRP_BILLING_MIN,         -- GrpTRESMins       */
        WAIT_ASSOC_GRP_BILLING_RUN_MIN,     -- GrpTRESRunMins    */
        WAIT_ASSOC_MAX_BILLING_PER_JOB,     -- MaxTRESPerJob     */
        WAIT_ASSOC_MAX_BILLING_PER_NODE,    -- MaxTRESPerNode    */
        WAIT_ASSOC_MAX_BILLING_MINS_PER_JOB, -- MaxTRESMinsPerJob */

        WAIT_QOS_GRP_BILLING,               -- GrpTRES           */
        WAIT_QOS_GRP_BILLING_MIN,           -- GrpTRESMins       */
        WAIT_QOS_GRP_BILLING_RUN_MIN,       -- GrpTRESRunMins    */
        WAIT_QOS_MAX_BILLING_PER_JOB,       -- MaxTRESPerJob     */
        WAIT_QOS_MAX_BILLING_PER_NODE,      -- MaxTRESPerNode    */
        WAIT_QOS_MAX_BILLING_PER_USER,      -- MaxTRESPerUser    */
        WAIT_QOS_MAX_BILLING_MINS_PER_JOB,  -- MaxTRESMinsPerJob */
        WAIT_QOS_MAX_BILLING_PER_ACCT,      -- MaxTRESPerAcct    */
        WAIT_QOS_MIN_BILLING,               -- MinTRESPerJob     */

        WAIT_RESV_DELETED             -- Reservation was deleted */
                         );

   type Extended_State is (
             Ext_RUNNING, -- being executed
             Ext_SUSPENDED, -- held after it has been started
             Ext_Transit, -- being started or stopped
             Ext_PREEMPTED, -- superceded by a higher-priority job
             Ext_DEADLINE, -- terminated on deadline
             Ext_PRIORITY, -- waiting for a higher-priority job
             Ext_DEPENDENCY, -- waiting for a dependency
             Ext_ARRAY_TASK_LIMIT, -- too many jobs from the same
                                   -- array are already running
             Ext_RESOURCES, -- waiting for resources
             Ext_TIME, -- waiting for a specific begin time
             Ext_HELD_USER, -- held by user
             Ext_PENDING, -- pending (with no known reason)
             Ext_Others -- waiting for other reasons
           );
   -- this combines basic job states and reasonsc
   type Extended_State_Count is array (Extended_State) of Natural;
   -- how many jobs are there in each state

   type Scheduler is (Backfill, Main);
   -- type of scheduler that may evaluate a given job
   -- @value Backfill the backfill scheduler
   -- @value Main the main scheduler
   type Job is new Loggers.Logger with private;
   -- opaque data type representing a job; the library may record
   -- errors that can later be retrieved by the application
   type Cursor is private;
   -- a cursor pointing to a job
   function Element (Position : Cursor) return Job;
   -- retrieve the job pointed to by a cursor
   -- @param Position the cursor to query
   -- @return the job pointed to by Position
   -- @exception Constraint_Error if Position doesn't point to any job
   function Has_Element (Position : Cursor) return Boolean;
   -- check if Position points to any job
   -- @param Position the cursor to query
   -- @return True iff Position points to any job
   function First return Cursor;
   -- @return the first job in the internal list
   procedure Next (Position : in out Cursor);
   -- advance a cursor by one job
   -- @param Position on entry, the cursor to advance; on exit,
   -- points one job further

   procedure Iterate (Process : not null access procedure (J : Job));
   -- call an arbitrary procedure for each job in the internal list
   -- @param Process the procedure to call
   -- @param J the job under consideration
   procedure Sort (By, Direction : String);
   -- sort the internal list
   -- @param By the column to sort by
   -- @param Direction determines the sort order; may be "inc", "dec",
   -- or ""; if "dec", sort in decreasing order, otherwise, sort
   -- increasing
   function Get_Account (J : Job) return String;
   -- return the account of the job
   -- @param J the job to query
   -- @return the account of the job
   function Get_Alloc_Node (J : Job) return String;
   -- return the allocation node (submit node) of the job
   -- @param J the job to query
   -- @return the allocation node of the job
   function Get_Command (J : Job) return String;
   -- return the command to be executed by the job
   -- @param J the job to query
   -- @return the command to be executed by the job
   function Has_Admin_Comment (J : Job) return Boolean;
   -- check whether the job has an admin comment
   -- @param J the job to query
   -- @return True iff J has an admin comment
   function Get_Admin_Comment (J : Job) return String;
   -- return the admin comment of the job
   -- @param J the job to query
   -- @return the admin comment of the job
   function Has_Comment (J : Job) return Boolean;
   -- check whether the job has a comment
   -- @param J the job to query
   -- @return True iff J has a comment
   function Get_Comment (J : Job) return String;
   -- return the comment of the job
   -- @param J the job to query
   -- @return the comment of the job
   function Get_CPUs (J : Job) return Natural;
   -- return the number of CPUs requested by the job
   -- @param J the job to query
   -- @return the number of CPUs requested by the job
   function Get_Node_Number (J : Job) return Natural;
   -- return the number of nodes requested by the job
   -- @param J the job to query
   -- @return the number of nodes requested by the job
   function Get_Dependency (J : Job) return String;
   -- return the dependencies of a job
   -- @param J the job to query
   -- @return the dependencies of J
   function Get_Gres (J : Job) return String;
   -- return the generic resources requested
   -- @param J the job to query
   -- @return the generic resources of J
   function Get_Group (J : Job) return User_Name;
   -- return the user group of a job
   -- @param J the job to query
   -- @return the group under which J runs
   function Get_ID (J : Job) return Positive;
   -- return the ID of a job
   -- @param J the job to query
   -- @return the job ID of J
   function Get_Name (J : Job) return String;
   -- return the name of a job
   -- @param J the job to query
   -- @return the name of J
   function Get_Batch_Host (J : Job) return Node_Name;
   -- return the main execution node of a job, i.e. the node on which
   -- its batch script is executed
   -- @param J the job to query
   -- @return the batch host of J
   function Get_Nodes (J : Job) return Hostlist;
   -- return the nodes allocated to a job
   -- @param J the job to query
   -- @return a list of nodes allocated to J
   function Has_Node (J : Job; Nodename : Node_Name) return Boolean;
   -- check whether a given node has been allocated to a job
   -- @param J the job to query
   -- @param Nodename the node to ask for
   -- @return True iff Nodename has been allocated to J
   function Get_Owner (J : Job) return User_Name;
   -- return the owner of a job
   -- @param J the job to query
   -- @return the owner of J
   function Get_Partition (J : Job) return String;
   -- return the partition of a job
   -- @param J the job to query
   -- @return the partition of J
   function Get_Priority (J : Job) return Natural;
   -- return the priority of a job
   -- @param J the job to query
   -- @return the priority of J
   function Get_Project (J : Job) return String;
   -- return the project under which a job runs
   -- @param J the job to query
   -- @return the project of J
   function Get_QOS (J : Job) return String;
   -- return the QOS requested by a job
   -- @param J the job to query
   -- @return the QOS of J

   function Get_Reservation (J : Job) return String;
   -- return the reservation requested by a job
   -- @param J the job to query
   -- @return the name of the reservation requested by J
   function Has_Start_Time (J : Job) return Boolean;
   -- check whether a job has a start time (i.e. either it has been
   -- started, or it has a backfill reservation)
   -- @param J the job to query
   -- @return True iff J has a start time
   function Get_Start_Time (J : Job) return Ada.Calendar.Time;
   -- return the start time of a job
   -- @param J the job to query
   -- @return the start time of J
   function Get_End_Time (J : Job) return Ada.Calendar.Time;
   -- return the end time of a job; usually, this will be an estimate
   -- based on the start time and the wall time requested
   -- @param J the job to query
   -- @return the end time of J

   function Walltime (J : Job) return Duration;
   -- how long has a job been running?
   -- @param J the job to query
   -- @return the elapsed walltime of J
   function Get_Sched_Eval_Time (J : Job) return Ada.Calendar.Time;
   -- when has a job last been evaluated by the scheduler
   -- @param J the job to query
   -- @return the time J has last been evaluated
   function Get_State (J : Job) return String;
   -- return the state of J as a string
   -- @param J the job to query
   -- @return the state of J as a string
   function Get_State (J : Job) return states;
   -- return the state of J
   -- @param J the job to query
   -- @return the state of J
   function Get_Extended_State (J : Job) return Extended_State;
   -- return the extended state of a job
   -- @param J the job to query
   -- @return the extended state of J
   function Get_State_Description (J : Job) return String;
   -- return optional details for the state reason of a job
   -- @param J the job to query
   -- @return the state description of J
   function Get_State_Reason (J : Job) return state_reasons;
   -- return the reason a job is in a given state
   -- @param J the job to query
   -- @return the reason J is in a given state
   function Get_Submission_Time (J : Job) return Ada.Calendar.Time;
   -- return the time a job was submitted
   -- @param J the job to query
   -- @return the time J was submitted
   function Get_Tasks (J : Job) return Natural;
   -- return how many tasks a job has requested
   -- @param J the job to query
   -- @return the number of tasks J has requested
   function Get_Tasks_Per_Core (J : Job) return Positive;
   -- return how many tasks per core a job has requested
   -- @param J the job to query
   -- @return how many tasks per core J has requested
   function Get_Tasks_Per_Node (J : Job) return Positive;
   -- return how many tasks per node a job has requested
   -- @param J the job to query
   -- @return how many tasks per node J has requested
   function Get_Tasks_Per_Socket (J : Job) return Positive;
   -- return how many tasks per socket a job has requested
   -- @param J the job to query
   -- @return how many tasks per socket J has requested
   function Get_Tasks_Per_Board (J : Job) return Positive;
   -- return how many tasks per board a job has requested
   -- @param J the job to query
   -- @return how many tasks per board J has requested
   function Get_Std_In (J : Job) return String;
   -- return the name of the file connected to the job's std in
   -- @param J the job to query
   -- @return the name of the file connected to J's std in
   function Get_Std_Out (J : Job) return String;
   -- return the name of the file connected to the job's std out
   -- @param J the job to query
   -- @return the name of the file connected to J's std out
   function Get_Std_Err (J : Job) return String;
   -- return the name of the file connected to the job's std err
   -- @param J the job to query
   -- @return the name of the file connected to J's std err
   function Get_TRES_Request (J : Job) return String;
   -- return the trackable resources requested by a job
   -- @param J the job to query
   -- @return the trackable resources requested by J
   function Get_TRES_Per_Node (J : Job) return String;
   -- return the trackable resources allocated to a job on each node
   -- @param J the job to query
   -- @return the trackable resources allocated to J on each node
   function Get_TRES_Allocated (J : Job) return String;
   -- return the trackable resources allocated to a job
   -- @param J the job to query
   -- @return the trackable resources allocated to J
   function Get_Working_Directory (J : Job) return String;
   -- return the working directory of a job
   -- @param J the job to query
   -- @return the working directory of J
   function Has_Error (J : Job) return Boolean;
   -- check whether a job is in an error state; this is meant to work
   -- similar to SGE's E state, but for now, always returns False
   -- @param J the job to query
   -- @return False
   function Is_Pending (J : Job) return Boolean;
   -- check whether a job is waiting for execution
   -- @param J the job to query
   -- @return True iff J is waiting for execution
   function Is_Running (J : Job) return Boolean;
   -- check whether a job is running
   -- @param J the job to query
   -- @return True iff J is running
   function Has_Share (J : Job) return Boolean;
   -- check if a job can share its nodes with other jobs
   -- @param J the job to query
   -- @returb True iff J can share its nodes with other jobs
   function Quota_Inhibited (J : Job) return Boolean;
   -- check if a job cannot start due to resource quotas
   -- @param J the job to query
   -- @return True iff resource quotas prevent J from starting
   function Last_Scheduler (J : Job) return Scheduler;
   -- return the scheduler (main or backfill) that last evaluated a
   -- job
   -- @param J the job to query
   -- @return which scheduler last evaluated J

   function Get_Array_Job_ID (J : Job) return Natural;
   -- get the ID of the job array
   -- @param J the job to query
   -- @return the ID of the jobb array to which J belongs
   function Get_Array_Task_ID (J : Job) return Natural;
   -- return the task ID of this job
   -- @param J the job to query
   -- @return the task ID of J
   function Get_Array_Max_Tasks (J : Job) return Natural;
   -- how many tasks of this job array may run in parallel?
   -- @param J the job to query
   -- @return the maximum number of tasks in the array that J belongs
   -- to
   function Get_Array_Tasks (J : Job) return String;
   -- return the task IDs in this array
   -- @param J the job to query
   -- @return the task IDs in the array that J belongs to

   procedure Pick (Selector : not null access function (J : Job) return Boolean);
   -- Reduce the list of jobs by keeping only selected jobs. If no
   -- jobs have been loaded, get all jobs from Slurm first.
   -- @param Selector a boolean function that will be called once for
   -- all jobs in the list. After Pick returns, the list will contain
   -- exactly those jobs for which Selector returned True.
   -- @param J the job under consideration

   procedure Load_Jobs;
   -- load all jobs from Slurm
   procedure Load_User (User : String);
   -- load jobs of a specific user from Slurm
   -- @param User the user whose jobs should be loaded

   procedure Get_Summary (Jobs, Tasks : out State_Count);
   -- get a summary for all loaded jobs
   -- @param Jobs the number of jobs in each state
   -- @param Tasks the number of tasks in each state
   procedure Get_Extended_Summary (Jobs, Tasks : out Extended_State_Count);
   -- get a summary by Extended_States for all jobs
   -- @param Jobs the number of jobs in each extended state
   -- @param Tasks the number of tasks in each extended state
   function Get_Job (ID : Natural) return Job;
   -- find a job based on its ID
   -- @param ID the ID of the job to return
   -- @return the job record with the given ID

   function Get_Backfill return Ada.Calendar.Time;
   -- when has the backfill scheduler last run?
   -- @return the time that the backfill scheduler has run

private
   function Precedes_By_ID (Left, Right : Job) return Boolean;
   function Precedes_By_Submission (Left, Right : Job) return Boolean;
   function Precedes_By_State (Left, Right : Job) return Boolean;
   function Precedes_By_Starttime (Left, Right : Job) return Boolean;
   function Precedes_By_Walltime (Left, Right : Job) return Boolean;
   function Precedes_By_Total_Priority (Left, Right : Job) return Boolean;
   function Precedes_By_Owner (Left, Right : Job) return Boolean;

   type Job is new Slurm.Loggers.Logger with record
      Comment, Admin_Comment : Unbounded_String;
      Account     : Unbounded_String;
      Alloc_Node             : Unbounded_String;
      Array_Job_ID, Array_Task_ID, Array_Max_Tasks : Natural;
      Array_Tasks : Unbounded_String;
      Batch_Host             : Node_Name;
      Bit_Flags : Interfaces.Unsigned_64;
      Gres        : Unbounded_String;
      ID          : Positive;
      Name        : Unbounded_String;
      Owner       : User_Name;
      Group       : User_Name;
      Priority    : Natural;
      Project     : Unbounded_String;
      QOS         : Unbounded_String;
      Has_Start_Time : Boolean;
      Start_Time : Ada.Calendar.Time;
      Has_End_Time : Boolean;
      End_Time       : Ada.Calendar.Time;
      Last_Sched_Eval : Ada.Calendar.Time;
      Shared         : Boolean;
      State          : states;
      State_Desc     : Unbounded_String;
      State_Reason   : state_reasons;
      Submission_Time : Ada.Calendar.Time;
      Tasks           : Natural;
      CPUs            : Natural;
      Node_Number     : Natural;
      Dependency      : Unbounded_String;
      Nodes           : Hostlist;
      Partition       : Unbounded_String;
      Reservation     : Unbounded_String;
      Std_In, Std_Err, Std_Out : Unbounded_String;
      Directory                : Unbounded_String;
      Command                  : Unbounded_String;
      TRES_Request, TRES_Allocated : Unbounded_String;
      TRES_Per_Node   : Unbounded_String;
      Tasks_Per_Core, Tasks_Per_Node,
      Tasks_Per_Socket, Tasks_Per_Board : Natural;
   end record;

   package Sortable_Lists is new ada.Containers.Doubly_Linked_Lists (Element_type => Job);

   package Lists is new ada.Containers.Ordered_Maps (Element_Type => Job,
                                                     Key_Type     => Positive);
   type Cursor is new Sortable_Lists.Cursor;

   package Sorting_By_ID is new Sortable_Lists.Generic_Sorting
     ("<" => Precedes_By_ID);
   package Sorting_By_Submission is new Sortable_Lists.Generic_Sorting
     ("<" => Precedes_By_Submission);
   package Sorting_By_State is new Sortable_Lists.Generic_Sorting
     ("<" => Precedes_By_State);
   package Sorting_By_Starttime is new Sortable_Lists.Generic_Sorting
     ("<" => Precedes_By_Starttime);
   package Sorting_By_Walltime is new Sortable_Lists.Generic_Sorting
     ("<" => Precedes_By_Walltime);
   package Sorting_By_Total_Priority is new Sortable_Lists.Generic_Sorting
     ("<" => Precedes_By_Total_Priority);
   package Sorting_By_Owner is new Sortable_Lists.Generic_Sorting
     ("<" => Precedes_By_Owner);

end Slurm.Jobs;
