package Slurm is
  -- The top level package of the Slurm interface library

   Internal_Error : exception;
   -- An exception that is raised whenever the Slurm API returns a
   -- fatal error

   procedure Init;
   -- Sets up the Slurm API; this is called automatically during
   -- elaboration

   procedure Finish;
   -- Destroy any remaining structures and cleanly end use of the API;
   -- not strictly necessary
end Slurm;
