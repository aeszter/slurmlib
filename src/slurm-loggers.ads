with Slurm.Utils;
package Slurm.Loggers is
  -- Record errors that occur at the library level for later retrieval
  -- by the application,
  --
   type Logger is tagged private;
   -- This type can be used as an ancestor for all objects that may
   -- encounter non-fatal errors. It provides facilities to record
   -- such errors (at the library level), then allow the application
   -- to retrieve them and display them in an appropriate way.

   function Has_Errors (Object : Logger) return Boolean;
   -- Did a given object record any errors?
   -- @param Object the object to query
   -- @return True iff errors have been recorded

   function Errors_Exist return Boolean;
   -- Are there (global) errors at the Type level?
   -- @return True iff errors have been recorded

   procedure Record_Error (Object : in out Logger; Message : String);
   -- Record an error for a given object
   -- @param Object the object to which the error pertains
   -- @param Message the error message to record

   procedure Record_Error (Message : String);
   -- Record an error that does not pertain to a specific object
   -- @param Message the error message to record

   procedure Iterate_Errors (Object  : Logger;
                             Process : not null access procedure (Message : String));
   -- Call an application-defined procedure for every error recorded
   -- by a given object
   -- @param Object the object to query
   -- @param Process the procedure to call
   -- @param Message Process will be called once for every error by
   -- Object, and Message will contain the respective error message

   procedure Iterate_Errors (Process : not null access procedure (Message : String));
   -- Call an application-defined procedure for every recorded global error
   -- @param Process the procedure to call
   -- @param Message Process will be called once for every global
   -- error, and Message will contain the respective error message

private
   type Logger is tagged record
      Error_Log : Slurm.Utils.String_List;
   end record;
end Slurm.Loggers;
