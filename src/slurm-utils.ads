with POSIX.C;
with Ada.Containers;
with Ada.Containers.Ordered_Sets;
with Ada.Containers.Doubly_Linked_Lists;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Interfaces.C;

package Slurm.Utils is
  -- @summary Routines and types used in several parts of the library, or
  -- pertaining to the library as a whole

   Version : String := "v0.29";
   -- The library version should be incremented with every release

   type User_Name is new String (1 .. 8);
   -- A string type to store user names; the length is chosen to
   -- accommodate classic Unix names.

   type Usage_Number is delta 0.0001 digits 18;
   -- A general fixed-point type mainly for CPU time, memory and so
   -- on

   type Usage_Integer is range 0 .. 10 ** 12;
   -- A general integer type similar to Usage_Number, used when
   -- there is no fractional part

   type Gigs is delta 0.001 digits 9;
   -- Memory measured in Gigabytes

   function To_String (Memory : Gigs) return String;
   -- Output a memory size, without unit
   -- @param Memory the quantity to print
   -- @return the input quantity formatted as a string

   Infinite_Duration : constant Duration := Duration'Last;
   -- a special value corresponding to an infinite Duration

   Invalid_Duration : constant Duration := Duration'Pred (Infinite_Duration);
   -- a special value corresponding to an undefined Duration

   package String_Lists is
     new Ada.Containers.Doubly_Linked_Lists (Element_Type => Unbounded_String);
   -- A simple string container

   package String_Sets is
     new Ada.Containers.Ordered_Sets (Element_Type => Unbounded_String);
   -- A simple sorted string container

   subtype String_List is String_Lists.List;
   function To_String_Set (Source  : String) return String_Sets.Set;
   -- Convert a list of comma-separated phrases into a Set
   -- @param Source the list to convert
   -- @return the resulting Set

   function To_User_Name (User : String) return User_Name;
   -- Convert a regular string to a User_Name
   -- @param User the user name as a string
   -- @return the user name as a User_Name, truncated to fit

   function To_String (User : User_Name) return String;
   -- Convert a User_Name to a regular string
   -- @param User the User_Name to convert
   -- @return the contents of User as a regular string

   function Convert_User (UID : Interfaces.C.unsigned) return User_Name;
   -- Return the name of a user identified by ID
   -- @param UID a user id
   -- @return the user name belonging to the UID
   -- @exception Constraint_Error if there is no record with the given UID

   function To_UID (Name : String) return POSIX.C.uid_t;
   -- Return the ID of a user identified by name
   -- @param Name a user name
   -- @return the user ID belonging to the name
   -- @exception Constraint_Error if there is no record with the given
   -- name

end Slurm.Utils;
