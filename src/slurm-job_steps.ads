with Slurm.Loggers;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Ordered_Maps;
with Ada.Calendar;

-- @summary This package deals with job steps, i.e. sub-units of jobs
-- launched by srun.
package Slurm.Job_Steps is

   type Job_Step is new Loggers.Logger with private;
   -- an opaque type representing one jobs step
   type List is private;
   -- a collection of job steps
   type Cursor is private;
   -- a position inside a collection of job steps

   type Step_Kind is (Normal, Pending, Container, Batch, Interactive);
   -- a job step can be one of various types
   -- @value Normal a normal job step launched by srun
   -- @value Pending a job step not yet launched
   -- @value Container a job step representing a container
   -- @value Batch the first job step implicitly launched by sbatch
   -- @value Interactive an interactive job
   function Element (Position : Cursor) return Job_Step;
   -- the job step pointed to by a cursor
   -- @param Position the cursor to evaluate
   -- @return the job step represented by Position
   -- @exception Constraint_Error if Position does not represent any
   -- job step
   function Has_Element (Position : Cursor) return Boolean;
   -- check whether Position does in fact represent a job step
   -- @param Position the cursor to evaluate
   -- @return True iff Position does represent a job step
   function First (Collection : List) return Cursor;
   -- return the first entry in a collection
   -- @param Collection the collection to check
   -- @return a Cursor representing the first element in Collection,
   -- if any
   function Is_Empty (Collection : List) return Boolean;
   -- check whether there are any jobs steps in a collection
   -- @param Collection the collection to evaluate
   -- @return False if there is at least one job step in the
   -- collection
   procedure Next (Position : in out Cursor);
   -- advance a cursor to the next job step
   -- @param Position when called, the cursor to advance; upon return,
   -- points one job step further
   procedure Insert (Collection : in out List;
                     Key        : Integer;
                     Item       : Job_Step);
   -- add another job step to a collection
   -- @param Collection the collection to add to
   -- @param Key the ID of the job step to add
   -- @param Item the job step to add
   procedure Iterate (Collection : List;
                      Process    : not null access procedure (Position : Cursor));
   -- call a given procedure for every job step in a list
   -- @param Collection the list to operate on
   -- @param Process the Procedure to call
   -- @param Position for each job step in Collection, Process will be
   -- called once, and Position will contain a cursor pointing to that
   -- job step
   function Load_Job_Steps (Job : Natural) return List;
   -- read a list of job steps from Slurm
   -- @param Job the ID of the job whose steps shall be read
   -- @return a list of job steps

   function Get_CPUs (S : Job_Step) return Natural;
   -- the number of CPUs allocated to a job step
   -- @param S the job step to query
   -- @return the number of CPUs of S
   function Get_ID (S : Job_Step) return Integer;
   -- the ID of a job step
   -- @param S the job step to query
   -- @return the ID CPUs of S
   function Get_Full_ID (S : Job_Step) return String;
   -- the ID or, for special job steps, the kind
   -- @param S the job step to query
   -- @return a string representation of the ID or kind of S
   function Get_Kind (S : Job_Step) return Step_Kind;
   -- the kind (type) of a job step
   -- @param S the job step to query
   -- @return the kind of S
   function Get_Nodes (S : Job_Step) return String;
   -- the nodes allocated to a job step
   -- @param S the job step to query
   -- @return the nodes of S
   function Get_Start (S : Job_Step) return Ada.Calendar.Time;
   -- the start time of a job step
   -- @param S the job step to query
   -- @return the start time of S
   function Get_Submit_Line (S : Job_Step) return String;
   -- the command that created a job step
   -- @param S the job step to query
   -- @return the submit line of S

private
   type Job_Step is new Slurm.Loggers.Logger with record
      CPUs : Natural;
      ID   : Integer;
      Kind : Step_Kind;
      Start_Time : Ada.Calendar.Time;
      Submit_Line : Unbounded_String;
      Nodes : Unbounded_String;
   end record;

   package Lists is new Ada.Containers.Ordered_Maps
     (Element_Type => Job_Step,
          Key_Type => Integer);

   type Cursor is new Lists.Cursor;
   type List is record
      Container : Lists.Map;
   end record;

end Slurm.Job_Steps;
