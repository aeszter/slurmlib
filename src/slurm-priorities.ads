with Ada.Containers.Ordered_Maps;

-- @summary This package collects routines and data pertaining to the order in
-- which Slurm starts jobs.
package Slurm.Priorities is

   type Priority is record
      Total, Age, Fairshare, Job_Size, Partition : Natural;
      QOS : Natural;
   end record;
   -- the priority of a job, including all contributions
   -- @field Total the overall priority; the job with the highes
   -- priority will be started first
   -- @field Age contribution of the time the job has spent in the
   -- queue
   -- @field Fairshare contribution of the user's pas resource usage
   -- @field Job_Size contribution of the amount of resources the job
   -- requests
   -- @field Partition contribution of the partition the job requests
   -- @field QOS contribution of the QOS the job requests

   Undefined : constant Priority := (others => 0);
   -- a priority that has not been initialized

   Format_Error : exception; -- unexpected data from sprio

   procedure Load;
   -- read priority values for all jobs from Slurm
   -- @exception Format_Error if sprio outputs data in an unexpected
   -- form
   function Get_Priority (J : Positive) return Priority;
   -- get priority values for a given job
   -- @param J the ID of the job whose priority is to be returned
   -- @return the priority of J; if the job cannot be found, Undefined
   -- is returned.
   -- @exception Format_Error if sprio outputs data in an unexpected
   -- form; Get_Priority will not raise an exception if Load has been
   -- called before

   -- A collection of job priorities
   package Maps is new ada.Containers.Ordered_Maps (Key_Type     => Positive,
                                                    Element_Type => Priority);
end Slurm.Priorities;
