with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Calendar;
with Slurm.Loggers;
with Slurm.Node_Properties; use Slurm.Node_Properties;
with Slurm.Partitions; use Slurm.Partitions;
with Slurm.Utils; use Slurm.Utils;
with Slurm.Gres;
with Ada.Containers.Ordered_Maps;
with Ada.Containers.Ordered_Sets;
with Slurm.Tres;
with Slurm.Hostlists; use Slurm.Hostlists;
with Interfaces;

-- @summary This package provides types and subprograms that work with
-- nodes
package Slurm.Nodes is

   type states is (
      NODE_STATE_UNKNOWN, -- an unknown state
      NODE_STATE_DOWN,    -- node isn't usable
      NODE_STATE_IDLE,    -- node is ready to take jobs
      NODE_STATE_ALLOCATED, -- node has a job
      NODE_STATE_ERROR,   -- node has experienced an error
      NODE_STATE_MIXED,   -- node has a job, but some CPUs are still idle
      NODE_STATE_FUTURE   -- node is reserved for future used
      );
   -- states a node might be in
   type State_Count is array (states) of Natural;
   -- how many nodes are there in each of the states

   function To_String (S : states) return String;
   -- convert a state to a string
   -- @param S the state to convert
   -- @return a string representation of S

   type Node is new Loggers.Logger with private;
   -- an opaque type that represents a node. The library can record
   -- error for later retrieval by the application.
   type List is private;
   -- a list of nodes
   type Cursor is private;
   -- a pointer into a list
   function Element (Position : Cursor) return Node;
   function Has_Element (Position : Cursor) return Boolean;
   function First (Collection : List) return Cursor;
   procedure Next (Position : in out Cursor);
   procedure Append (Collection : in out List; Item : Node);
   procedure Iterate (Collection : List;
                      Process    : not null access procedure (Position : Cursor));
   function Load_Nodes return List;
   function Select_Nodes (Source   : List;
                          Selector : not null access function (Item : Node) return Boolean)
                          return List;
   function To_String (Source : List) return String;

   type Percent is range 0 .. 100;

   function Color_Class (P : Percent) return String;
   function Color_Class (Load : Node_Properties.Load) return String;

   function Get_Architecture (N : Node) return String;
   function Get_Boards (N : Node) return Positive;
   function Get_Boot_Time (N : Node) return Ada.Calendar.Time;
   function Get_Last_Busy (N : Node) return Ada.Calendar.Time;
   function Get_Cores_Per_Socket (N : Node) return Positive;

   function Get_CPUs (N : Node) return Positive;
   function Get_Free_CPUs (N : Node) return Natural;
   function Get_Used_CPUs (N : Node) return Natural;
   function Get_Features (N : Node) return String;
   function Get_Free_Memory (N : Node) return String;
   function Get_Load (N : Node) return Usage_Number;
   function Get_Memory (N : Node) return String;
   function Get_Name (N : Node) return Node_Name;
   function Get_OS (N : Node) return String;
   function Get_Owner (N : Node) return User_Name;
   function Get_Partitions (N : Node) return String;
   function Get_Sockets (N : Node) return Positive;
   function Get_State (N : Node) return states;
   function Get_State (N : Node) return String;
   function Get_Reason (N : Node) return String;
   function Get_Reason_User (N : Node) return User_Name;
   function Get_Reason_Time (N : Node) return Ada.Calendar.Time;
   function Get_Comment (N : Node) return String;
   function Get_Start_Time (N : Node) return Ada.Calendar.Time;
   function Get_Threads_Per_Core (N : Node) return Positive;
   function Get_Tmp_Total (N : Node) return Gigs;
   function Get_TRES (N : Node) return Slurm.Tres.List;
   function Get_Version (N : Node) return String;
   function Get_Weight (N : Node) return Integer;
   function Get_Properties (N : Node) return Set_Of_Properties;
   procedure Get_Summary (List : Slurm.Nodes.List; S : out State_Count);
   -- get a summary for all loaded nodes
   -- @param S the number of nodes in each state

   function Is_Draining (N : Node) return Boolean;
   function Is_Completing (N : Node) return Boolean;
   function Is_Not_Responding (N : Node) return Boolean;
   function Is_Power_Saving (N : Node) return Boolean;
   function Is_Failing (N : Node) return Boolean;
   function Is_Powering_Up (N : Node) return Boolean;
   function Is_Maintenance (N : Node) return Boolean;

   function Load_Per_Core (N : in out Node) return Load;
   function Mem_Percentage (N : in out Node) return Percent;

   procedure Iterate_Jobs (N       : Node;
                           Process : not null access procedure (ID : Positive; N : Node));
   procedure Iterate_Partitions (N : Node; Process : not null access procedure (P : Partition));
   procedure Iterate_GRES (N       : Node;
                           Process : not null access procedure (R : Slurm.Gres.Resource));
   procedure Iterate_GRES_Drain (N       : Node;
                                 Process : not null access procedure (R : Slurm.Gres.Resource));
   procedure Iterate_GRES_Used (N       : Node;
                                 Process : not null access procedure (R : Slurm.Gres.Resource));

   function Get_Node (Collection : List; Name : String) return Node;

   procedure Add_Jobs (To : in out Node);
   procedure Add_Jobs (To : in out List);

private

   subtype state_type  is Interfaces.Unsigned_32;
   package Job_Lists is new ada.Containers.Ordered_Sets (Element_Type => Positive);

   type Node is new Loggers.Logger with record
      Architecture     : Unbounded_String;
      Boards           : Natural;
      Boot_Time,
      Last_Busy        : Ada.Calendar.Time;
      Cores_Per_Socket : Natural;
      Sockets          : Natural;
      Threads_Per_Core : Natural;
      Used_CPUs        : Natural := 0;
      Start_Time       : Ada.Calendar.Time;
      Load             : Usage_Number;
      Free_Memory      : Gigs;
      GRES_Drain,
      GRES_Used        : Slurm.Gres.List;
      Name             : Node_Name;
      Basic_State      : states;
      Full_State       : state_type;
      OS               : Unbounded_String;
      Owner            : User_Name;
      Partitions       : Unbounded_String;
      Reason, Comment  : Unbounded_String;
      Reason_Time      : Ada.Calendar.Time;
      Reason_User      : User_Name;
      Tmp_Total        : Gigs;
      Weight           : Natural;
      Version          : Unbounded_String;
      Jobs             : Job_Lists.Set;
      Properties       : Set_Of_Properties;

   end record;

   package Lists is new ada.Containers.Ordered_Maps (Element_Type => Node,
                                                     Key_Type => Node_Name);
   type Cursor is new Lists.Cursor;
   type List is record
      Container : Lists.Map;
   end record;

end Slurm.Nodes;
