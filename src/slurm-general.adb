with POSIX;
with POSIX.C; use POSIX.C;

with Slurm.Low_Level.slurm_slurm_h; use Slurm.Low_Level.slurm_slurm_h;

package body Slurm.General is

   function  API_Version return Natural is
   begin
      return Natural (slurm_api_version);
   end API_Version;
end Slurm.General;
