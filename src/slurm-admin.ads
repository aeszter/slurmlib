with POSIX.C;

-- @summary this package collects routines that require special
-- privileges; typically, these alter the state of the cluster (as
-- opposed to routines that provide information only)
package Slurm.Admin is

   procedure Release_Job (ID : Positive);
   -- release a job from a hold, i.e. make it eligible for execution
   -- @param ID the ID of the job to be released
   -- @exception Slurm_Error is raised if the current user does not
   -- have sufficient privileges, or if an error was signaled by Slurm
   procedure Kill_Job (ID : Positive);
   -- remove a job from the queue
   -- @param ID the ID of the job to be removed
   -- @exception Slurm_Error is raised if the current user does not
   -- have sufficient privileges, or if an error was signaled by Slurm
   procedure Down_Node (Name, Reason : String; uid : POSIX.C.uid_t);
   -- set a node as down, killing all its jobs and preventing it from
   -- taking new ones
   -- @param Name the name of the node
   -- @param Reason a string explaining why the node is marked down
   -- @param uid the ID of the user performing this action
   -- @exception Slurm_Error is raised if the current user does not
   -- have sufficient privileges, or if an error was signaled by Slurm
   procedure Drain_Node (Name, Reason : String; UID : POSIX.C.uid_t);
   -- set a node as draining, preventing it from taking new jobs but
   -- leaving running jobs alone
   -- @param Name the name of the node
   -- @param Reason a string explaining why the node is marked
   -- draining
   -- @param UID the ID of the user performing this action
   -- @exception Slurm_Error is raised if the current user does not
   -- have sufficient privileges, or if an error was signaled by Slurm
   procedure Resume_Node (Name : String);
   -- return a node to service after it has been marked as down or
   -- draining, allowing it to take jobs again
   -- @param Name the name of the node
   -- @exception Slurm_Error is raised if the current user does not
   -- have sufficient privileges, or if an error was signaled by Slurm
   procedure Undrain_Node (Name : String);
   -- remove the draining state from a node but do not return it to
   -- service if it is down
   -- @param Name the name of the node
   -- @exception Slurm_Error is raised if the current user does not
   -- have sufficient privileges, or if an error was signaled by Slurm

end Slurm.Admin;
