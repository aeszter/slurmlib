with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers;
with Ada.Containers.Ordered_Sets;

-- @summary this package provides subprograms that deal with trackable
-- resources
package Slurm.Tres is
   type Resource is record
      Name   : Unbounded_String;
      Number : Positive;
   end record;
   -- one trackable resource
   -- @field Name the name of the resource
   -- @field Number the value of the resource

   function "<" (Left, Right : Resource) return Boolean;
   -- lexicographically compare resources
   -- @param Left one trackable resource
   -- @param Right another trackable resource
   -- @return True iff Left is lexicographically ordered before Right
   function ">" (Left, Right : Resource) return Boolean;
   -- lexicographically compare resources
   -- @param Left one trackable resource
   -- @param Right another trackable resource
   -- @return True iff Left is lexicographically ordered after Right
   overriding function "=" (Left, Right : Resource) return Boolean;
   -- lexicographically compare resources
   -- @param Left one trackable resource
   -- @param Right another trackable resource
   -- @return True iff Left and Right are the same, but allow for 1% difference
   -- if we are looking at memory resources

   package Lists is new Ada.Containers.Ordered_Sets (Resource);
   -- this package provides containers that store resources
   subtype List is Lists.Set;

   function Init (Source : String) return List;
   -- create a list of resources based on a string received from Slurm
   -- @param Source the string containing a description of the TRES
   -- @return a list of resources
   function New_Resource (Source : String) return Resource;
   -- create a single resource based on a string received from Slurm
   -- @param Source the string containing a description of one
   -- trackable resource
   -- @return a resource conforming to Source
   function "<" (Left, Right : List) return Boolean;
   -- compare two lists of TRES according to an arbitrary order
   -- @param Left one List of TRES
   -- @param Right another List of TRES
   -- @return True iff Left is ordered before Right
   function ">" (Left, Right : List) return Boolean;
   -- compare two lists of TRES according to an arbitrary order
   -- @param Left one List of TRES
   -- @param Right another List of TRES
   -- @return True iff Left is ordered after Right
   function "=" (Left, Right : List) return Boolean;
   -- check if two lists of TRES are equal
   -- @param Left one List of TRES
   -- @param Right another List of TRES
   -- @return True iff Left are equal, i.e. if the describe the same
   -- resources

private

end Slurm.Tres;
