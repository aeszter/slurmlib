with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers;
with Ada.Containers.Ordered_Sets;

-- @summary This package provides an interface to slurm hostlists that
-- ultimately deal with slurm's compact "node12-[01-20]" notation.
package Slurm.Hostlists is

   type Node_Name is new Unbounded_String;
   Null_Node_Name : Node_Name := Node_Name (Null_Unbounded_String);

   overriding function "<" (Left, Right : Node_Name) return Boolean;
   -- lexicographically order node names
   -- @param Left one node name
   -- @param Right another node name
   -- @return True iff Left is ordered before Right
   pragma Inline ("<");
   overriding function "=" (Left, Right : Node_Name) return Boolean;
   -- compare node names
   -- @param Left one node name
   -- @param Right another node name
   -- @return True iff Left an Right are identical
   pragma Inline ("=");

   overriding function To_String (Source : Node_Name) return String;
   -- convert a node name to an ordinary string
   -- @param Source the node name to convert
   -- @return a String containing the node name
   function To_Node_Name (Source : String) return Node_Name;
   -- convert a string containing a node name to a Node_Name
   -- @param Source the string to convert
   -- @return the Node_Name

   package Name_Sets is
     new Ada.Containers.Ordered_Sets (Element_Type => Node_Name);
   subtype Hostlist is Name_Sets.Set;

   function To_Hostlist (Source : String) return Hostlist;
   -- convert a string containing a list of nodes into a list
   -- @param Source the string to convert
   -- @return a host list containing the nodes in Source

   function To_String (Source : Hostlist) return String;
   -- convert a list of nodes into a string using ranges
   -- for a compact notation
   -- @param Source the host list to convert
   -- @return a string containg the nodes in Source

   function To_Ranged_String (Source : String) return String;
   -- convert a list of nodes into a string using ranges
   -- for a compact notation
   -- @param Source a string containing the host list to convert
   -- @return a string containg the nodes in Source

private

end Slurm.Hostlists;
