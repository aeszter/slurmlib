with Ada.Calendar.Conversions;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C.Pointers;
with POSIX;
with POSIX.C; use POSIX.C;
with Slurm.Errors;
with Ada.Exceptions; use Ada.Exceptions;
with System.Address_To_Access_Conversions;

with Slurm.Low_Level.slurm_slurm_h; use Slurm.Low_Level.slurm_slurm_h;
with Slurm.Low_Level.bits_stdint_uintn_h; use Slurm.Low_Level.bits_stdint_uintn_h;
with System;
with Slurm.Low_Level.Utils; use Slurm.Low_Level.Utils;
with Interfaces; use Interfaces;

package body Slurm.Jobs is

   function getgrgid (c_gid : gid_t) return group_ptr;
   pragma Import (C, getgrgid, "getgrgid");

   type Enum_To_State_Map is array (Interfaces.C.unsigned range 0 .. 11) of states;
   Enum_To_State : constant Enum_To_State_Map :=
                     (0 => JOB_PENDING,
                      1 => JOB_RUNNING,
                      2 => JOB_SUSPENDED,
                      3 => JOB_COMPLETE,
                      4 => JOB_CANCELLED,
                      5 => JOB_FAILED,
                      6 => JOB_TIMEOUT,
                      7 => JOB_NODE_FAIL,
                      8 => JOB_PREEMPTED,
                      9 => JOB_BOOT_FAIL,
                      10 => JOB_DEADLINE,
                      11 => JOB_OOM);

   type Enum_To_Reason_Map is array (Interfaces.C.unsigned range
                                     0 .. 198) of state_reasons;
   Enum_To_Reason : constant Enum_To_Reason_Map :=
                      (
0 => WAIT_NO_REASON,
1 => WAIT_PRIORITY,
2 => WAIT_DEPENDENCY,
3 => WAIT_RESOURCES,
4 => WAIT_PART_NODE_LIMIT,
5 => WAIT_PART_TIME_LIMIT,
6 => WAIT_PART_DOWN,
7 => WAIT_PART_INACTIVE,
8 => WAIT_HELD,
9 => WAIT_TIME,
10 => WAIT_LICENSES,
11 => WAIT_ASSOC_JOB_LIMIT,
12 => WAIT_ASSOC_RESOURCE_LIMIT,
13 => WAIT_ASSOC_TIME_LIMIT,
14 => WAIT_RESERVATION,
15 => WAIT_NODE_NOT_AVAIL,
16 => WAIT_HELD_USER,
17 => WAIT_FRONT_END,
18 => FAIL_DEFER,
19 => FAIL_DOWN_PARTITION,
20 => FAIL_DOWN_NODE,
21 => FAIL_BAD_CONSTRAINTS,
22 => FAIL_SYSTEM,
23 => FAIL_LAUNCH,
24 => FAIL_EXIT_CODE,
25 => FAIL_TIMEOUT,
26 => FAIL_INACTIVE_LIMIT,
27 => FAIL_ACCOUNT,
28 => FAIL_QOS,
29 => WAIT_QOS_THRES,
30 => WAIT_QOS_JOB_LIMIT,
31 => WAIT_QOS_RESOURCE_LIMIT,
32 => WAIT_QOS_TIME_LIMIT,
33 => WAIT_BLOCK_MAX_ERR,
34 => WAIT_BLOCK_D_ACTION,
35 => WAIT_CLEANING,
36 => WAIT_PROLOG,
37 => WAIT_QOS,
38 => WAIT_ACCOUNT,
39 => WAIT_DEP_INVALID,
40 => WAIT_QOS_GRP_CPU,
41 => WAIT_QOS_GRP_CPU_MIN,
42 => WAIT_QOS_GRP_CPU_RUN_MIN,
43 => WAIT_QOS_GRP_JOB,
44 => WAIT_QOS_GRP_MEM,
45 => WAIT_QOS_GRP_NODE,
46 => WAIT_QOS_GRP_SUB_JOB,
47 => WAIT_QOS_GRP_WALL,
48 => WAIT_QOS_MAX_CPU_PER_JOB,
49 => WAIT_QOS_MAX_CPU_MINS_PER_JOB,
50 => WAIT_QOS_MAX_NODE_PER_JOB,
51 => WAIT_QOS_MAX_WALL_PER_JOB,
52 => WAIT_QOS_MAX_CPU_PER_USER,
53 => WAIT_QOS_MAX_JOB_PER_USER,
54 => WAIT_QOS_MAX_NODE_PER_USER,
55 => WAIT_QOS_MAX_SUB_JOB,
56 => WAIT_QOS_MIN_CPU,
57 => WAIT_ASSOC_GRP_CPU,
58 => WAIT_ASSOC_GRP_CPU_MIN,
59 => WAIT_ASSOC_GRP_CPU_RUN_MIN,
60 => WAIT_ASSOC_GRP_JOB,
61 => WAIT_ASSOC_GRP_MEM,
62 => WAIT_ASSOC_GRP_NODE,
63 => WAIT_ASSOC_GRP_SUB_JOB,
64 => WAIT_ASSOC_GRP_WALL,
65 => WAIT_ASSOC_MAX_JOBS,
66 => WAIT_ASSOC_MAX_CPU_PER_JOB,
67 => WAIT_ASSOC_MAX_CPU_MINS_PER_JOB,
68 => WAIT_ASSOC_MAX_NODE_PER_JOB,
69 => WAIT_ASSOC_MAX_WALL_PER_JOB,
70 => WAIT_ASSOC_MAX_SUB_JOB,
71 => WAIT_MAX_REQUEUE,
72 => WAIT_ARRAY_TASK_LIMIT,
73 => WAIT_BURST_BUFFER_RESOURCE,
74 => WAIT_BURST_BUFFER_STAGING,
75 => FAIL_BURST_BUFFER_OP,
76 => WAIT_POWER_NOT_AVAIL,
77 => WAIT_POWER_RESERVED,
78 => WAIT_ASSOC_GRP_UNK,
79 => WAIT_ASSOC_GRP_UNK_MIN,
80 => WAIT_ASSOC_GRP_UNK_RUN_MIN,
81 => WAIT_ASSOC_MAX_UNK_PER_JOB,
82 => WAIT_ASSOC_MAX_UNK_PER_NODE,
83 => WAIT_ASSOC_MAX_UNK_MINS_PER_JOB,
84 => WAIT_ASSOC_MAX_CPU_PER_NODE,
85 => WAIT_ASSOC_GRP_MEM_MIN,
86 => WAIT_ASSOC_GRP_MEM_RUN_MIN,
87 => WAIT_ASSOC_MAX_MEM_PER_JOB,
88 => WAIT_ASSOC_MAX_MEM_PER_NODE,
89 => WAIT_ASSOC_MAX_MEM_MINS_PER_JOB,
90 => WAIT_ASSOC_GRP_NODE_MIN,
91 => WAIT_ASSOC_GRP_NODE_RUN_MIN,
92 => WAIT_ASSOC_MAX_NODE_MINS_PER_JOB,
93 => WAIT_ASSOC_GRP_ENERGY,
94 => WAIT_ASSOC_GRP_ENERGY_MIN,
95 => WAIT_ASSOC_GRP_ENERGY_RUN_MIN,
96 => WAIT_ASSOC_MAX_ENERGY_PER_JOB,
97 => WAIT_ASSOC_MAX_ENERGY_PER_NODE,
98 => WAIT_ASSOC_MAX_ENERGY_MINS_PER_JOB,
99 => WAIT_ASSOC_GRP_GRES,
100 => WAIT_ASSOC_GRP_GRES_MIN,
101 => WAIT_ASSOC_GRP_GRES_RUN_MIN,
102 => WAIT_ASSOC_MAX_GRES_PER_JOB,
103 => WAIT_ASSOC_MAX_GRES_PER_NODE,
104 => WAIT_ASSOC_MAX_GRES_MINS_PER_JOB,
105 => WAIT_ASSOC_GRP_LIC,
106 => WAIT_ASSOC_GRP_LIC_MIN,
107 => WAIT_ASSOC_GRP_LIC_RUN_MIN,
108 => WAIT_ASSOC_MAX_LIC_PER_JOB,
109 => WAIT_ASSOC_MAX_LIC_MINS_PER_JOB,
110 => WAIT_ASSOC_GRP_BB,
111 => WAIT_ASSOC_GRP_BB_MIN,
112 => WAIT_ASSOC_GRP_BB_RUN_MIN,
113 => WAIT_ASSOC_MAX_BB_PER_JOB,
114 => WAIT_ASSOC_MAX_BB_PER_NODE,
115 => WAIT_ASSOC_MAX_BB_MINS_PER_JOB,
116 => WAIT_QOS_GRP_UNK,
117 => WAIT_QOS_GRP_UNK_MIN,
118 => WAIT_QOS_GRP_UNK_RUN_MIN,
119 => WAIT_QOS_MAX_UNK_PER_JOB,
120 => WAIT_QOS_MAX_UNK_PER_NODE,
121 => WAIT_QOS_MAX_UNK_PER_USER,
122 => WAIT_QOS_MAX_UNK_MINS_PER_JOB,
123 => WAIT_QOS_MIN_UNK,
124 => WAIT_QOS_MAX_CPU_PER_NODE,
125 => WAIT_QOS_GRP_MEM_MIN,
126 => WAIT_QOS_GRP_MEM_RUN_MIN,
127 => WAIT_QOS_MAX_MEM_MINS_PER_JOB,
128 => WAIT_QOS_MAX_MEM_PER_JOB,
129 => WAIT_QOS_MAX_MEM_PER_NODE,
130 => WAIT_QOS_MAX_MEM_PER_USER,
131 => WAIT_QOS_MIN_MEM,
132 => WAIT_QOS_GRP_ENERGY,
133 => WAIT_QOS_GRP_ENERGY_MIN,
134 => WAIT_QOS_GRP_ENERGY_RUN_MIN,
135 => WAIT_QOS_MAX_ENERGY_PER_JOB,
136 => WAIT_QOS_MAX_ENERGY_PER_NODE,
137 => WAIT_QOS_MAX_ENERGY_PER_USER,
138 => WAIT_QOS_MAX_ENERGY_MINS_PER_JOB,
139 => WAIT_QOS_MIN_ENERGY,
140 => WAIT_QOS_GRP_NODE_MIN,
141 => WAIT_QOS_GRP_NODE_RUN_MIN,
142 => WAIT_QOS_MAX_NODE_MINS_PER_JOB,
143 => WAIT_QOS_MIN_NODE,
144 => WAIT_QOS_GRP_GRES,
145 => WAIT_QOS_GRP_GRES_MIN,
146 => WAIT_QOS_GRP_GRES_RUN_MIN,
147 => WAIT_QOS_MAX_GRES_PER_JOB,
148 => WAIT_QOS_MAX_GRES_PER_NODE,
149 => WAIT_QOS_MAX_GRES_PER_USER,
150 => WAIT_QOS_MAX_GRES_MINS_PER_JOB,
151 => WAIT_QOS_MIN_GRES,
152 => WAIT_QOS_GRP_LIC,
153 => WAIT_QOS_GRP_LIC_MIN,
154 => WAIT_QOS_GRP_LIC_RUN_MIN,
155 => WAIT_QOS_MAX_LIC_PER_JOB,
156 => WAIT_QOS_MAX_LIC_PER_USER,
157 => WAIT_QOS_MAX_LIC_MINS_PER_JOB,
158 => WAIT_QOS_MIN_LIC,
159 => WAIT_QOS_GRP_BB,
160 => WAIT_QOS_GRP_BB_MIN,
161 => WAIT_QOS_GRP_BB_RUN_MIN,
162 => WAIT_QOS_MAX_BB_PER_JOB,
163 => WAIT_QOS_MAX_BB_PER_NODE,
164 => WAIT_QOS_MAX_BB_PER_USER,
165 => WAIT_QOS_MAX_BB_MINS_PER_JOB,
166 => WAIT_QOS_MIN_BB,
167 => FAIL_DEADLINE,
168 => WAIT_QOS_MAX_BB_PER_ACCT,
169 => WAIT_QOS_MAX_CPU_PER_ACCT,
170 => WAIT_QOS_MAX_ENERGY_PER_ACCT,
171 => WAIT_QOS_MAX_GRES_PER_ACCT,
172 => WAIT_QOS_MAX_NODE_PER_ACCT,
173 => WAIT_QOS_MAX_LIC_PER_ACCT,
174 => WAIT_QOS_MAX_MEM_PER_ACCT,
175 => WAIT_QOS_MAX_UNK_PER_ACCT,
176 => WAIT_QOS_MAX_JOB_PER_ACCT,
177 => WAIT_QOS_MAX_SUB_JOB_PER_ACCT,
178 => WAIT_PART_CONFIG,
179 => WAIT_ACCOUNT_POLICY,
180 => WAIT_FED_JOB_LOCK,
181 => FAIL_OOM,
182 => WAIT_PN_MEM_LIMIT,
183 => WAIT_ASSOC_GRP_BILLING,
184 => WAIT_ASSOC_GRP_BILLING_MIN,
185 => WAIT_ASSOC_GRP_BILLING_RUN_MIN,
186 => WAIT_ASSOC_MAX_BILLING_PER_JOB,
187 => WAIT_ASSOC_MAX_BILLING_PER_NODE,
188 => WAIT_ASSOC_MAX_BILLING_MINS_PER_JOB,
189 => WAIT_QOS_GRP_BILLING,
190 => WAIT_QOS_GRP_BILLING_MIN,
191 => WAIT_QOS_GRP_BILLING_RUN_MIN,
192 => WAIT_QOS_MAX_BILLING_PER_JOB,
193 => WAIT_QOS_MAX_BILLING_PER_NODE,
194 => WAIT_QOS_MAX_BILLING_PER_USER,
195 => WAIT_QOS_MAX_BILLING_MINS_PER_JOB,
196 => WAIT_QOS_MAX_BILLING_PER_ACCT,
197 => WAIT_QOS_MIN_BILLING,
198 => WAIT_RESV_DELETED

                      );

   BACKFILL_SCHED : constant Interfaces.Unsigned_64 := Shift_Left (1, 32);

   procedure Init (J : out Job; Ptr : access job_info);
   procedure Build_Map (Buffer : aliased access job_info_msg);
   procedure Build_Sorted_List;

   Default_Terminator : job_info;
   pragma Warnings (off, Default_Terminator);
   --  this is a dummy. The job list given by the slurm API isn't
   --  terminated, and subprograms from node_info_ptrs (below) that
   --  use termination must not be called

   type job_array is array (uint32_t range <>) of
     aliased job_info;
   package job_info_ptrs is new Interfaces.C.Pointers
     (Index         => uint32_t,
      Element       => job_info,
      Element_Array => job_array,
     Default_Terminator => Default_Terminator);
   subtype job_info_ptr is job_info_ptrs.Pointer;

   type job_info_msg_ptr is access job_info_msg;
   package job_info_msg_ptr_ptrs is new System.Address_To_Access_Conversions
     (Object => job_info_msg_ptr);
   subtype job_info_msg_ptr_ptr is job_info_msg_ptr_ptrs.Object_Pointer;

   The_Map : Lists.Map;
   The_List : Sortable_Lists.List;
   Last_Backfill : Ada.Calendar.Time := Convert_Time (0);

   Loaded : Boolean := False;
   -- Have Jobs been loaded? Then The_Map and The_List can be used
   -- as is. Otherwise, Load_Jobs first.

   procedure Build_Map (Buffer : aliased access job_info_msg) is
      use job_info_ptrs;
      Job_Ptr : job_info_ptr;
      J       : Job;

   begin
      The_Map.Clear;
      The_List.Clear;
      Last_Backfill := Convert_Time (Buffer.last_backfill);
      Job_Ptr := Buffer.job_array;
      for I in 1 .. Buffer.record_count loop
         Init (J, Job_Ptr);
         The_Map.Insert (Get_ID (J), J);
         Increment (Job_Ptr);
      end loop;
      slurm_free_job_info_msg (Buffer);

      Build_Sorted_List;
      Loaded := True;
   end Build_Map;

   procedure Build_Sorted_List is
      procedure Add_One (Position : Lists.Cursor);

      procedure Add_One (Position : Lists.Cursor) is
      begin
         The_List.Append (New_Item => Lists.Element (Position));
      end Add_One;

   begin
      The_List.Clear;
      The_Map.Iterate (Add_One'Access);
   end Build_Sorted_List;

   overriding function Element (Position : Cursor) return Job is
   begin
      return Sortable_Lists.Element (Sortable_Lists.Cursor (Position));
   end Element;

   function First return Cursor is
   begin
      if not Loaded then
         Load_Jobs;
      end if;
      return Cursor (The_List.First);
   end First;

   function Get_Account (J : Job) return String is
   begin
      return To_String (J.Account);
   end Get_Account;

   function Get_Admin_Comment (J : Job) return String is
   begin
      return To_String (J.Admin_Comment);
   end Get_Admin_Comment;

   function Get_Alloc_Node (J : Job) return String is
   begin
      return To_String (J.Alloc_Node);
   end Get_Alloc_Node;

   function Get_Array_Job_ID (J : Job) return Natural is
   begin
      return J.Array_Job_ID;
   end Get_Array_Job_ID;

   function Get_Array_Max_Tasks (J : Job) return Natural is
   begin
      return J.Array_Max_Tasks;
   end Get_Array_Max_Tasks;

   function Get_Array_Task_ID (J : Job) return Natural is
   begin
      return J.Array_Task_ID;
   end Get_Array_Task_ID;

   function Get_Array_Tasks (J : Job) return String is
   begin
      return To_String (J.Array_Tasks);
   end Get_Array_Tasks;

   function Get_Backfill return Ada.Calendar.Time is
   begin
      return Last_Backfill;
   end Get_Backfill;

   function Get_Batch_Host (J : Job) return Node_Name is
   begin
      return J.Batch_Host;
   end Get_Batch_Host;

   function Get_Command (J : Job) return String is
   begin
      return To_String (J.Command);
   end Get_Command;

   function Get_Comment (J : Job) return String is
   begin
      return To_String (J.Comment);
   end Get_Comment;

   function Get_CPUs (J : Job) return Natural is
   begin
      return J.CPUs;
   end Get_CPUs;

   function Get_Dependency (J : Job) return String is
   begin
      return To_String (J.Dependency);
   end Get_Dependency;

   function Get_End_Time (J : Job) return Ada.Calendar.Time is
   begin
      return J.End_Time;
   end Get_End_Time;

   function Get_Extended_State (J : Job) return Extended_State is
   begin
      case J.State is
         when JOB_PENDING =>
            case J.State_Reason is
               when WAIT_PRIORITY =>
                  return Ext_PRIORITY;
               when WAIT_DEPENDENCY =>
                  return Ext_DEPENDENCY;
               when WAIT_ARRAY_TASK_LIMIT =>
                  return Ext_ARRAY_TASK_LIMIT;
               when WAIT_RESOURCES =>
                  return Ext_RESOURCES;
               when WAIT_TIME =>
                  return Ext_TIME;
               when WAIT_HELD_USER =>
                  return Ext_HELD_USER;
               when WAIT_NO_REASON =>
                  return Ext_PENDING;
               when others =>
                  return Ext_Others;
            end case;
         when JOB_RUNNING =>
            return Ext_RUNNING;
         when JOB_SUSPENDED =>
            return Ext_SUSPENDED;
         when JOB_PREEMPTED =>
            return Ext_PREEMPTED;
         when JOB_DEADLINE =>
            return Ext_DEADLINE;
         when others =>
            return Ext_Transit;
      end case;
   end Get_Extended_State;

   procedure Get_Extended_Summary (Jobs, Tasks : out Extended_State_Count) is
      procedure Increment (Position : Lists.Cursor);

      procedure Increment (Position : Lists.Cursor) is
         J : Job := Lists.Element (Position);
      begin
         Jobs (J.Get_Extended_State) :=  Jobs (J.Get_Extended_State) + 1;
         Tasks (J.Get_Extended_State) := Tasks (J.Get_Extended_State) + J.Tasks;
      end Increment;

   begin
      Jobs := (others => 0);
      Tasks := (others => 0);
      The_Map.Iterate (Increment'Access);
   end Get_Extended_Summary;

   function Get_Gres (J : Job) return String is
   begin
      return To_String (J.Gres);
   end Get_Gres;

   function Get_Group (J : Job) return User_Name is
   begin
      return J.Group;
   end Get_Group;

   function Get_ID (J : Job) return Positive is
   begin
      return J.ID;
   end Get_ID;

   function Get_Job (ID : Natural) return Job is
   begin
      if not Loaded then
         Load_Jobs;
      end if;
      return The_Map.Element (ID);
   exception
      when Constraint_Error =>
         raise Constraint_Error with "Job not found";
   end Get_Job;

   function Get_Name (J : Job) return String is
   begin
      return To_String (J.Name);
   end Get_Name;

   function Get_Node_Number (J : Job) return Natural is
   begin
      return J.Node_Number;
   end Get_Node_Number;

   function Get_Nodes (J : Job) return Slurm.Hostlists.Hostlist is
   begin
      return J.Nodes;
   end Get_Nodes;

   function Get_Owner (J : Job) return User_Name is
   begin
      return J.Owner;
   end Get_Owner;

   function Get_Partition (J : Job) return String is
   begin
      return To_String (J.Partition);
   end Get_Partition;

   function Get_Priority (J : Job) return Natural is
   begin
      return J.Priority;
   end Get_Priority;

   function Get_Project (J : Job) return String is
   begin
      return To_String (J.Project);
   end Get_Project;

   function Get_QOS (J : Job) return String is
   begin
      return To_String (J.QOS);
   end Get_QOS;

   function Get_Reservation (J : Job) return String is
   begin
      return To_String (J.Reservation);
   end Get_Reservation;

   function Get_Sched_Eval_Time (J : Job) return Ada.Calendar.Time is
   begin
      return J.Last_Sched_Eval;
   end Get_Sched_Eval_Time;

   function Get_Start_Time (J : Job) return Ada.Calendar.Time is
   begin
      return J.Start_Time;
   end Get_Start_Time;

   function Get_State (J : Job) return states is
   begin
      return J.State;
   end Get_State;

   function Get_State (J : Job) return String is
   begin
      return J.State'Img;
   end Get_State;

   function Get_State_Description (J : Job) return String is
   begin
      return To_String (J.State_Desc);
   end Get_State_Description;

   function Get_State_Reason (J : Job) return state_reasons is
   begin
      return J.State_Reason;
   end Get_State_Reason;

   function Get_Std_Err (J : Job) return String is
   begin
      return To_String (J.Std_Err);
   end Get_Std_Err;

   function Get_Std_In (J : Job) return String is
   begin
      return To_String (J.Std_In);
   end Get_Std_In;

   function Get_Std_Out (J : Job) return String is
   begin
      return To_String (J.Std_Out);
   end Get_Std_Out;

   function Get_Submission_Time (J : Job) return Ada.Calendar.Time is
   begin
      return J.Submission_Time;
   end Get_Submission_Time;

   procedure Get_Summary (Jobs, Tasks : out State_Count) is
      procedure Increment (Position : Lists.Cursor);

      procedure Increment (Position : Lists.Cursor) is
         J : Job := Lists.Element (Position);
      begin
         Jobs (J.State) :=  Jobs (J.State) + 1;
         Tasks (J.State) := Tasks (J.State) + J.Tasks;
      end Increment;

   begin
      Jobs := (others => 0);
      Tasks := (others => 0);
      The_Map.Iterate (Increment'Access);
   end Get_Summary;

   function Get_Tasks (J : Job) return Natural is
   begin
      return J.Tasks;
   end Get_Tasks;

   function Get_Tasks_Per_Board (J : Job) return Positive is
   begin
      return J.Tasks_Per_Board;
   end Get_Tasks_Per_Board;

   function Get_Tasks_Per_Core (J : Job) return Positive is
   begin
      return J.Tasks_Per_Core;
   end Get_Tasks_Per_Core;

   function Get_Tasks_Per_Node (J : Job) return Positive is
   begin
      return J.Tasks_Per_Node;
   end Get_Tasks_Per_Node;

   function Get_Tasks_Per_Socket (J : Job) return Positive is
   begin
      return J.Tasks_Per_Socket;
   end Get_Tasks_Per_Socket;

   function Get_TRES_Allocated (J : Job) return String is
   begin
      return To_String (J.TRES_Allocated);
   end Get_TRES_Allocated;

   function Get_TRES_Per_Node (J : Job) return String is
   begin
      return To_String (J.TRES_Per_Node);
   end Get_TRES_Per_Node;

   function Get_TRES_Request (J : Job) return String is
   begin
      return To_String (J.TRES_Request);
   end Get_TRES_Request;

   function Get_Working_Directory (J : Job) return String is
   begin
      return To_String (J.Directory);
   end Get_Working_Directory;

   function Has_Admin_Comment (J : Job) return Boolean is
   begin
      return J.Admin_Comment /= "";
   end Has_Admin_Comment;

   function Has_Comment (J : Job) return Boolean is
   begin
      return J.Comment /= "";
   end Has_Comment;

   overriding function Has_Element (Position : Cursor) return Boolean is
   begin
      return Sortable_Lists.Has_Element (Sortable_Lists.Cursor (Position));
   end Has_Element;

   function Has_Error (J : Job) return Boolean is
      pragma Unreferenced (J);
   begin
      return False; -- until we figure out what state is equivalent to sge's error state
   end Has_Error;

   function Has_Node (J : Job; Nodename : Node_Name) return Boolean is
   begin
      return J.Nodes.Contains (Nodename);
   end Has_Node;

   function Has_Share (J : Job) return Boolean is
   begin
      return J.Shared;
   end Has_Share;

   function Has_Start_Time (J : Job) return Boolean is
   begin
      return J.Has_Start_Time;
   end Has_Start_Time;

   procedure Init (J : out Job; Ptr : access job_info) is
      use Interfaces.C;
   begin
      -- set ID first as it's used as a key in the job map;
      -- otherwise, unhandled exceptions may prevent it
      -- from being set, leading to Key Already In Map
      -- later on
      J.ID := Integer (Ptr.all.job_id);
      J.Account := Convert_String (Ptr.all.account);
      J.Alloc_Node := Convert_String (Ptr.all.alloc_node);
      begin
         J.Array_Job_ID := Integer (Ptr.all.array_job_id);
         J.Array_Task_ID := Integer (Ptr.all.array_task_id);
         J.Array_Max_Tasks := Integer (Ptr.all.array_max_tasks);
         J.Array_Tasks := Convert_String (Ptr.all.array_task_str);
      exception
         when Constraint_Error =>
            J.Array_Task_ID := 0;
      end;
      J.Bit_Flags := Interfaces.Unsigned_64 (Ptr.all.bitflags);
      J.Gres := Convert_String (Ptr.all.gres_total);
      J.Name := Convert_String (Ptr.all.name);
      declare
         Given_Name : String := To_String (Ptr.all.user_name);
      begin
         if Given_Name /= "" then
            J.Owner := To_User_Name (Given_Name);
         else
            J.Owner := Convert_User (Ptr.all.user_id);
         end if;
      exception
            when Constraint_Error =>
            J.Owner := To_User_Name ("unknown");
      end;
      J.Priority := Natural (Ptr.all.priority);
      J.Project := Convert_String (Ptr.all.wckey);
      J.QOS := Convert_String (Ptr.all.qos);
      if Ptr.all.shared = 0 then
         J.Shared := False;
      else
         J.Shared := True;
      end if;
      J.Start_Time := Ada.Calendar.Conversions.To_Ada_Time (Interfaces.C.long (Ptr.all.start_time));
      if Ptr.all.start_time = 0 then
         J.Has_Start_Time := False;
      else
         J.Has_Start_Time := True;
      end if;
      J.End_Time := Ada.Calendar.Conversions.To_Ada_Time (Interfaces.C.long (Ptr.all.end_time));
      J.Last_Sched_Eval := Ada.Calendar.Conversions.To_Ada_Time
        (Interfaces.C.long (Ptr.all.last_sched_eval));
      if Ptr.all.end_time = 0 then
         J.Has_End_Time := False;
      else
         J.Has_End_Time := True;
      end if;
      J.State := Enum_To_State (Ptr.all.job_state and JOB_STATE_BASE);
      J.Submission_Time := Convert_Time (Ptr.all.submit_time);
      declare
      begin
         J.Tasks := Integer (Ptr.all.num_tasks);
      exception
         when Constraint_Error =>
            null; -- cancelled jobs report an illegal value here (-2)
      end;
      J.Tasks_Per_Core := Natural (Ptr.all.ntasks_per_core);
      J.Tasks_Per_Node := Natural (Ptr.all.ntasks_per_node);
      J.Tasks_Per_Socket := Natural (Ptr.all.ntasks_per_socket);
      J.Tasks_Per_Board := Natural (Ptr.all.ntasks_per_board);
      J.CPUs := Integer (Ptr.all.num_cpus);
      J.Node_Number := Integer (Ptr.all.num_nodes);
      J.Dependency := Convert_String (Ptr.all.dependency);
      declare
         gr_entry : group_ptr;
      begin
         gr_entry := getgrgid (gid_t (Ptr.all.group_id));
         if gr_entry = null
         then
            raise Constraint_Error;
         end if;
         J.Group := To_User_Name (POSIX.To_String (Form_POSIX_String (gr_entry.all.gr_name)));
      end;
      J.Nodes := Slurm.Hostlists.To_Hostlist (To_String (Ptr.all.nodes));
      J.Batch_Host := Node_Name (Convert_String (Ptr.all.batch_host));
      J.Partition := Convert_String (Ptr.all.partition);
      J.Reservation := Convert_String (Ptr.all.resv_name);
      J.State_Desc := Convert_String (Ptr.all.state_desc);
      J.State_Reason := Enum_To_Reason (Ptr.all.state_reason);
      J.Std_In := Convert_String (Ptr.all.std_in);
      J.Std_Out := Convert_String (Ptr.all.std_out);
      J.Std_Err := Convert_String (Ptr.all.std_err);
      J.Admin_Comment := Convert_String (Ptr.all.admin_comment);
      J.Comment := Convert_String (Ptr.all.comment);
      J.Command := Convert_String (Ptr.all.command);
      J.Directory := Convert_String (Ptr.all.work_dir);
      J.TRES_Request := Convert_String (Ptr.all.tres_req_str);
      J.TRES_Per_Node := Convert_String (Ptr.all.tres_per_node);
      J.TRES_Allocated := Convert_String (Ptr.all.tres_alloc_str);
   exception
      when E : others =>
         Record_Error (Object => J, Message => Exception_Message (E));
   end Init;

   function Is_Pending (J : Job) return Boolean is
   begin
      return J.State = JOB_PENDING;
   end Is_Pending;

   function Is_Running (J : Job) return Boolean is
   begin
      return J.State = JOB_RUNNING;
   end Is_Running;

   procedure Iterate (Process : not null access procedure (J : Job)) is
      procedure Wrapper (Position : Sortable_Lists.Cursor);

      procedure Wrapper (Position : Sortable_Lists.Cursor) is
      begin
         Process (Sortable_Lists.Element (Position));
      end Wrapper;
   begin
      if not Loaded then
         Load_Jobs;
      end if;
      The_List.Iterate (Wrapper'Access);
   end Iterate;

   function Last_Scheduler (J : Job) return Scheduler is
   begin
      if (J.Bit_Flags and BACKFILL_SCHED) /= 0 then
         return Backfill;
      else
         return Main;
      end if;
   end Last_Scheduler;

   procedure Load_Jobs is
      use Slurm.Errors;
      use Interfaces.C;
      use job_info_msg_ptr_ptrs;
      E : Error;
      Buffer : aliased job_info_msg_ptr;
   begin
      if slurm_load_jobs (update_time       => 0,
                          job_info_msg_pptr => To_Address (Buffer'Unchecked_Access),
                           show_flags        => 0) /= 0
      then
         E := Get_Last_Error;
         case E is
            when Protocol_Version_Error =>
               raise Internal_Error with "Incompatible protocol version";
            when Socket_Timeout =>
               raise Internal_Error with "Couldn't contact slurm controller";
            when others =>
               raise Constraint_Error with Get_Error (E);
         end case;
      end if;
      Build_Map (Buffer);
   end Load_Jobs;

   procedure Load_User (User : String) is
      use Slurm.Errors;
      use Interfaces.C;
      uid : uid_t := To_UID (User);
      E : Error;
      Buffer : aliased job_info_msg_ptr;
   begin
      if slurm_load_job_user
        (job_info_msg_pptr => job_info_msg_ptr_ptrs.To_Address (Buffer'Unchecked_Access),
         user_id           => Interfaces.C.unsigned (uid),
         show_flags         => 0) /= 0
      then
         E := Get_Last_Error;
         case E is
            when Protocol_Version_Error =>
               raise Internal_Error with "Incompatible protocol version";
            when Socket_Timeout =>
               raise Internal_Error with "Couldn't contact slurm controller";
            when others =>
               raise Constraint_Error with Get_Error (E);
         end case;
      end if;
      Build_Map (Buffer);
   end Load_User;

   overriding procedure Next (Position : in out Cursor) is
   begin
      Sortable_Lists.Next (Sortable_Lists.Cursor (Position));
   end Next;

   procedure Pick (Selector : not null access function (J : Job) return Boolean) is
      procedure Conditional_Copy (Position : Lists.Cursor);
      Result : Lists.Map;

      procedure Conditional_Copy (Position : Lists.Cursor) is
         J : Job := Lists.Element (Position);
      begin
         if Selector (J) then
            Result.Insert (Get_ID (J), J);
         end if;
      end Conditional_Copy;

   begin
      if not Loaded then
         Load_Jobs;
      end if;
      The_Map.Iterate (Conditional_Copy'Access);
      The_Map := Result;
      Build_Sorted_List;
   end Pick;

   function Precedes_By_ID (Left, Right : Job) return Boolean is
   begin
      return  Left.ID < Right.ID;
   end Precedes_By_ID;

   function Precedes_By_Owner (Left, Right : Job) return Boolean is
   begin
      return  Left.Owner < Right.Owner;
   end Precedes_By_Owner;

   function Precedes_By_Starttime (Left, Right : Job) return Boolean is
      use type Ada.Calendar.Time;
   begin
      return Left.Start_Time < Right.Start_Time;
   end Precedes_By_Starttime;

   function Precedes_By_State (Left, Right : Job) return Boolean is
   begin
      if Left.State < Right.State then
         return True;
      elsif Left.State > Right.State then
         return False;
      else
         return Left.State_Reason < Right.State_Reason;
      end if;
   end Precedes_By_State;

   function Precedes_By_Submission (Left, Right : Job) return Boolean is
      use type Ada.Calendar.Time;
   begin
      return Left.Submission_Time < Right.Submission_Time;
   end Precedes_By_Submission;

   function Precedes_By_Total_Priority (Left, Right : Job) return Boolean is
   begin
      return Left.Priority < Right.Priority;
   end Precedes_By_Total_Priority;

   function Precedes_By_Walltime (Left, Right : Job) return Boolean is
   begin
      return Left.Walltime < Right.Walltime;
   end Precedes_By_Walltime;

   function Quota_Inhibited (J : Job) return Boolean is
   begin
      return J.State_Reason = WAIT_ASSOC_GRP_JOB or else
        J.State_Reason = WAIT_QOS_GRP_JOB;
   end Quota_Inhibited;

   procedure Sort (By, Direction : String) is
   begin
      if not Loaded then
         Load_Jobs;
      end if;
      if By = "Number" then
         Sorting_By_ID.Sort (The_List);
      elsif By = "Owner" then
         Sorting_By_Owner.Sort (The_List);
      elsif By = "Submitted" then
         Sorting_By_Submission.Sort (The_List);
      elsif By = "State" then
         Sorting_By_State.Sort (The_List);
      elsif  By = "Walltime" then
         Sorting_By_Walltime.Sort (The_List);
      elsif  By = "Start" then
         Sorting_By_Starttime.Sort (The_List);
      elsif  By = "Total" then
         Sorting_By_Total_Priority.Sort (The_List);
      else
         raise Constraint_Error with "unsupported sort field " & By;
      end if;
      if Direction = "dec" then
         The_List.Reverse_Elements;
      end if;
   end Sort;

   function Walltime (J : Job) return Duration is
      use Ada.Calendar;
   begin
      if Is_Running (J) then
         return Ada.Calendar.Clock - J.Start_Time;
      else
         return Duration (0);
      end if;
   end Walltime;

end Slurm.Jobs;
