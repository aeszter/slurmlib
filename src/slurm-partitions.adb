with Interfaces.C.Pointers;
with Slurm.Low_Level.slurm_slurm_h; use Slurm.Low_Level.slurm_slurm_h;
with Slurm.Low_Level.bits_stdint_uintn_h; use Slurm.Low_Level.bits_stdint_uintn_h;
with Slurm.Errors;
with Slurm.General;
with System;
with System.Address_To_Access_Conversions;
with Slurm.Low_Level.Utils; use Slurm.Low_Level.Utils;
with Interfaces; use Interfaces;
with Ada.Strings;

package body Slurm.Partitions is
   PART_FLAG_DEFAULT        : constant Unsigned_16 := Shift_Left (1, 0);
   PART_FLAG_HIDDEN         : constant Unsigned_16 := Shift_Left (1, 1);
   PART_FLAG_NO_ROOT        : constant Unsigned_16 := Shift_Left (1, 2);
   PART_FLAG_ROOT_ONLY      : constant Unsigned_16 := Shift_Left (1, 3);
   PART_FLAG_REQ_RESV       : constant Unsigned_16 := Shift_Left (1, 4);
   PART_FLAG_LLN            : constant Unsigned_16 := Shift_Left (1, 5);
   PART_FLAG_EXCLUSIVE_USER : constant Unsigned_16 := Shift_Left (1, 6);
   PART_FLAG_PDOI           : constant Unsigned_16 := Shift_Left (1, 7);

   procedure Init (P : out Partition; Ptr : access partition_info);
   function Build_List (Buffer : aliased access partition_info_msg) return List;
   function Flag_Set (Flags : uint16_t; Test : Unsigned_16) return Boolean;

   Default_Terminator : partition_info;
   pragma Warnings (off, Default_Terminator);
   --  this is a dummy. The partition list given by the slurm API isn't
   --  terminated, and subprograms from partition_info_ptrs (below) that
   --  use termination must not be called

   type partition_array is array (uint32_t range <>) of aliased partition_info;
   package partition_info_ptrs is new Interfaces.C.Pointers
     (Index         => uint32_t,
      Element       => partition_info,
      Element_Array => partition_array,
     Default_Terminator => Default_Terminator);
   subtype partition_info_ptr is partition_info_ptrs.Pointer;

   type partition_info_msg_ptr is access partition_info_msg;
   package partition_info_msg_ptr_ptrs is new System.Address_To_Access_Conversions
     (Object => partition_info_msg_ptr);
   subtype partition_info_msg_ptr_ptr is partition_info_msg_ptr_ptrs.Object_Pointer;

   procedure Append (Collection : in out List; Item : Partition) is
      use Lists;
   begin
      Collection.Container.Include (Item.Name, Item);
   end Append;

   function Build_List (Buffer : aliased access partition_info_msg) return List is
      Part_Ptr : partition_info_ptr;
      P        : Partition;
      Result   : List;
   begin
      Part_Ptr := Buffer.partition_array;
      for I in 1 .. Buffer.record_count loop
         Init (P, Part_Ptr);
         Result.Container.Include (P.Name, P);
         partition_info_ptrs.Increment (Part_Ptr);
      end loop;
      slurm_free_partition_info_msg (Buffer);
      return Result;
   end Build_List;

   overriding function Element (Position : Cursor) return Partition is
   begin
      return Lists.Element (Lists. Cursor (Position));
   end Element;

   function First (Collection : List) return Cursor is
   begin
      return  Cursor (Lists.First (Collection.Container));
   end First;

   function Flag_Set (Flags : uint16_t; Test : Unsigned_16) return Boolean is
   begin
      return       (Unsigned_16 (Flags) and Test) /= 0;
   end Flag_Set;

   function Get_Accounts (P : Partition) return String is
   begin
      return  To_String (P.Accounts);
   end Get_Accounts;

   function Get_Allocation_Nodes (P : Partition) return String is
   begin
      return  To_String (P.Allocation_Nodes);
   end Get_Allocation_Nodes;

   function Get_Alternate (P : Partition) return String is
   begin
      return  To_String (P.Alternate);
   end Get_Alternate;

   function Get_Billing_Weights (P : Partition) return String is
   begin
      return To_String (P.Billing_Weights);
   end Get_Billing_Weights;

   function Get_CPUs_Per_GPU (P : Partition) return Natural is
   begin
      return P.CPUs_per_GPU;
   end Get_CPUs_Per_GPU;

   function Get_Default_Memory_Per_CPU (P : Partition) return Slurm.Utils.Gigs is
   begin
      return P.Default_Memory_Per_CPU;
   end Get_Default_Memory_Per_CPU;

   function Get_Default_Time (P : Partition) return Duration is
   begin
      return P.Default_Time;
   end Get_Default_Time;

   function Get_Deny_Accounts (P : Partition) return String is
   begin
      return  To_String (P.Deny_Accounts);
   end Get_Deny_Accounts;

   function Get_Deny_QOS (P : Partition) return String is
   begin
      return  To_String (P.Deny_QOS);
   end Get_Deny_QOS;

   function Get_Grace_Time (P : Partition) return Duration is
   begin
      return P.Grace_Time;
   end Get_Grace_Time;

   function Get_Groups (P : Partition) return String is
   begin
      return  To_String (P.Groups);
   end Get_Groups;

   function Get_Max_CPUs_Per_Node (P : Partition) return Integer is
   begin
      return P.Max_CPUs_Per_Node;
   end Get_Max_CPUs_Per_Node;

   function Get_Max_Memory_Per_CPU (P : Partition) return Slurm.Utils.Gigs is
   begin
      return P.Max_Memory_Per_CPU;
   end Get_Max_Memory_Per_CPU;

   function Get_Max_Nodes (P : Partition) return Integer is
   begin
      return P.Max_Nodes;
   end Get_Max_Nodes;

   function Get_Max_Share (P : Partition) return Integer is
   begin
      return P.Max_Share;
   end Get_Max_Share;

   function Get_Max_Time (P : Partition) return Duration is
   begin
      return P.Max_Time;
   end Get_Max_Time;

   function Get_Min_Nodes (P : Partition) return Integer is
   begin
      return P.Min_Nodes;
   end Get_Min_Nodes;

   function Get_Name (P : Partition) return String is
   begin
      return To_String (P.Name);
   end Get_Name;

   function Get_Node_Number (P : Partition) return Natural is
   begin
      return Length (P.Nodes);
   end Get_Node_Number;

   function Get_Nodes (P : Partition) return String is
   begin
      return To_String (P.Nodes);
   end Get_Nodes;

   function Get_Partition (Collection : List; Name : String) return Partition is
      Position : Cursor := First (Collection);
   begin
      while Has_Element (Position)
      loop
         if Element (Position).Name = Name then
            return Element (Position);
         else
            Next (Position);
         end if;
      end loop;
      raise Constraint_Error with "Partition not found";
   end Get_Partition;

   function Get_Preempt_Mode (P : Partition) return Preempt_Mode is
   begin
      return P.Preempt;
   end Get_Preempt_Mode;

   function Get_Preempt_Mode (P : Partition) return String is
   begin
      return P.Preempt'Img;
   end Get_Preempt_Mode;

   function Get_Priority_Job_Factor (P : Partition) return Integer is
   begin
      return P.Priority_Job_Factor;
   end Get_Priority_Job_Factor;

   function Get_Priority_Tier (P : Partition) return Integer is
   begin
      return P.Priority_Tier;
   end Get_Priority_Tier;

   function Get_QOS (P : Partition) return String is
   begin
      return To_String (P.QOS);
   end Get_QOS;

   function Get_QOS_Name (P : Partition) return String is
   begin
      return To_String (P.QOS_Name);
   end Get_QOS_Name;

   function Get_State_Up (P : Partition) return states is
   begin
      return P.State_Up;
   end Get_State_Up;

   function Get_Total_CPUs (P : Partition) return Natural is
   begin
      return P.Total_CPUs;
   end Get_Total_CPUs;

   function Get_Total_Nodes (P : Partition) return Natural is
   begin
      return P.Total_Nodes;
   end Get_Total_Nodes;

   function Get_TRES (P : Partition) return String is
   begin
      return To_String (P.TRES);
   end Get_TRES;

   function Has_CR_Board (P : Partition) return Boolean is
   begin
      return P.CR_Board;
   end Has_CR_Board;

   function Has_CR_Core (P : Partition) return Boolean is
   begin
      return P.CR_Core;
   end Has_CR_Core;

   function Has_CR_CPU (P : Partition) return Boolean is
   begin
      return P.CR_CPU;
   end Has_CR_CPU;

   function Has_CR_Memory (P : Partition) return Boolean is
   begin
      return P.CR_Memory;
   end Has_CR_Memory;

   function Has_CR_Socket (P : Partition) return Boolean is
   begin
      return P.CR_Socket;
   end Has_CR_Socket;

   overriding function Has_Element (Position : Cursor) return Boolean is
   begin
      return Lists.Has_Element (Lists.Cursor (Position));
   end Has_Element;

   procedure Init (P : out Partition; Ptr : access partition_info) is
      use Interfaces;
      use Interfaces.C;
   begin
      P.Allocation_Nodes := Convert_String (Ptr.all.allow_alloc_nodes);
      P.Accounts := Convert_String (Ptr.all.allow_accounts);
      P.Groups := Convert_String (Ptr.allow_groups);
      P.CPUs_per_GPU := 0;
      P.Deny_Accounts := Convert_String (Ptr.all.deny_accounts);
      P.QOS := Convert_String (Ptr.all.allow_qos);
      P.CR_CPU := Flag_Set (Ptr.all.cr_type, CR_CPU);
      P.CR_Socket := Flag_Set (Ptr.all.cr_type, CR_SOCKET);
      P.CR_Core := Flag_Set (Ptr.all.cr_type, CR_CORE);
      P.CR_Board := Flag_Set (Ptr.all.cr_type, CR_BOARD);
      P.CR_Memory := Flag_Set (Ptr.all.cr_type, CR_MEMORY);
      P.Deny_QOS := Convert_String (Ptr.all.deny_qos);
      P.Alternate := Convert_String (Ptr.all.alternate);
      P.Billing_Weights := Convert_String (Ptr.all.billing_weights_str);
      P.Default_Memory_Per_CPU := MiB_To_Gigs (Ptr.all.def_mem_per_cpu);
      P.Default_Time := Convert_Minutes (Ptr.all.default_time);

      P.Default := Flag_Set (Ptr.all.flags, PART_FLAG_DEFAULT);

      P.Hidden := Flag_Set (Ptr.all.flags, PART_FLAG_HIDDEN);
      P.No_Root := Flag_Set (Ptr.all.flags, PART_FLAG_NO_ROOT);
      P.Root_Only := Flag_Set (Ptr.all.flags, PART_FLAG_ROOT_ONLY);
            P.Reservation_Required := Flag_Set (Ptr.all.flags, PART_FLAG_REQ_RESV);
      P.Least_Loaded := Flag_Set (Ptr.all.flags, PART_FLAG_LLN);
      P.Exclusive_User := Flag_Set (Ptr.all.flags, PART_FLAG_EXCLUSIVE_USER);
                  P.Power_Down_On_Idle := Flag_Set (Ptr.all.flags, PART_FLAG_PDOI);

      P.Grace_Time := Convert_Seconds (Ptr.all.grace_time);
      P.Max_CPUs_Per_Node := Convert_Integer (Ptr.all.max_cpus_per_node);
      P.Max_Memory_Per_CPU := MiB_To_Gigs (Ptr.all.max_mem_per_cpu);
      P.Max_Nodes := Convert_Integer (Ptr.all.max_nodes);
      P.Max_Share := Integer (Ptr.all.max_share);
      P.Max_Time := Convert_Minutes (Ptr.all.max_time);
      P. Min_Nodes := Integer (Ptr.all.min_nodes);
      P.Name := Convert_String (Ptr.all.name);
      P.Nodes := Convert_String (Ptr.all.nodes);
      if Ptr.all.preempt_mode = PREEMPT_MODE_OFF then
         P.Preempt := preempt_off;
      elsif  Flag_Set (Ptr.all.preempt_mode, PREEMPT_MODE_SUSPEND) then
            P.Preempt := preempt_suspend;
      elsif Flag_Set (Ptr.all.preempt_mode, PREEMPT_MODE_REQUEUE) then
            P. Preempt := preempt_requeue;
      elsif Flag_Set (Ptr.all.preempt_mode, PREEMPT_MODE_CANCEL) then
         P.Preempt := preempt_cancel;
      elsif Flag_Set (Ptr.all.preempt_mode, PREEMPT_MODE_COND_OFF) then
         P.Preempt := preempt_off;
      elsif Flag_Set (Ptr.all.preempt_mode, PREEMPT_MODE_WITHIN) then
         P.Preempt := preempt_qos;
      elsif Flag_Set (Ptr.all.preempt_mode, PREEMPT_MODE_GANG) then
         P. Preempt := preempt_gang;
      else
         Record_Error (P, "unknown preempt mode" & Ptr.all.preempt_mode'Img);
         P. Preempt := preempt_off;
      end if;
      P.Priority_Job_Factor := Natural (Ptr.all.priority_job_factor);
      P.Priority_Tier := Integer (Ptr.all.priority_tier);
      P.QOS_Name := Convert_String (Ptr.all.qos_char);
      P.State_Up := states'Enum_Val (Ptr.all.state_up);
      P.Total_CPUs := Integer (Ptr.all.total_cpus);
      P.Total_Nodes := Integer (Ptr.all.total_nodes);
      P.TRES := Convert_String (Ptr.all.tres_fmt_str);
   end Init;

   function Is_Default (P : Partition) return Boolean is
   begin
      return P.Default;
   end Is_Default;

   function Is_Exclusive_User (P : Partition) return Boolean is
   begin
      return P.Exclusive_User;
   end Is_Exclusive_User;

   function Is_Hidden (P : Partition) return Boolean is
   begin
      return P.Hidden;
   end Is_Hidden;

   function Is_Idle_Power_Down (P : Partition) return Boolean is
   begin
      return P.Power_Down_On_Idle;
   end Is_Idle_Power_Down;

   function Is_Least_Loaded (P : Partition) return Boolean is
   begin
      return P.Least_Loaded;
   end Is_Least_Loaded;

   function Is_No_Root (P : Partition) return Boolean is
   begin
      return P.No_Root;
   end Is_No_Root;

   function Is_Root_Only (P : Partition) return Boolean is
   begin
      return P.Root_Only;
   end Is_Root_Only;

   procedure Iterate (Collection : List;
                      Process    : not null access procedure (Position : Cursor)) is
      procedure Wrapper (Position : Lists.Cursor);

      procedure Wrapper (Position : Lists.Cursor) is
      begin
         Process (Cursor (Position));
      end Wrapper;
   begin
      Collection.Container.Iterate (Wrapper'Access);
   end Iterate;

   function Load_Partitions return List is
      use Slurm.Errors;
      use Interfaces.C;

      E : Error;
      Buffer : aliased partition_info_msg_ptr;
   begin
      if Slurm.General.API_Version /= 16#270000# then
         raise Program_Error with "unsupported Slurm API version"
           & Slurm.General.API_Version'Img;
      end if;
      if slurm_load_partitions
        (update_time     => 0,
         part_buffer_ptr => partition_info_msg_ptr_ptrs.To_Address (Buffer'Unchecked_Access),
         show_flags       => 0) /= 0
      then
         E := Get_Last_Error;
         case E is
            when Protocol_Version_Error =>
               raise Internal_Error with "Incompatible protocol version";
            when Socket_Timeout =>
               raise Internal_Error with "Couldn't contact slurm controller";
            when others =>
               raise Constraint_Error with Get_Error (E);
         end case;
      end if;
      return Build_List (Buffer);
   end Load_Partitions;

   overriding procedure Next (Position : in out Cursor) is
   begin
      Lists.Next (Lists.Cursor (Position));
   end Next;

   function Requires_Reservation (P : Partition) return Boolean is
   begin
      return P.Reservation_Required;
   end Requires_Reservation;

end Slurm.Partitions;
