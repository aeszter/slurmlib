pragma Ada_2012;
with Ada.Strings;
with Ada.Strings.Fixed;
with Slurm.Errors;
with Interfaces.C;
with Interfaces.C.Pointers;
with System.Address_To_Access_Conversions;
with Slurm.Low_Level.slurm_slurm_h; use Slurm.Low_Level.slurm_slurm_h;
with Slurm.Low_Level.bits_stdint_uintn_h; use Slurm.Low_Level.bits_stdint_uintn_h;
with Slurm.Low_Level.Utils;

package body Slurm.Job_Steps is
   function Build_Map (Buffer : aliased access job_step_info_response_msg) return List;
   procedure Init (S : out Job_Step; Ptr : access job_step_info_t);

   Default_Terminator : job_step_info_t;
   pragma Warnings (off, Default_Terminator);
   --  this is a dummy. The job list given by the slurm API isn't
   --  terminated, and subprograms from node_step_info_ptrs (below) that
   --  use termination must not be called

   type job_step_array is array (uint32_t range <>) of
     aliased job_step_info_t;
   package job_step_info_ptrs is new Interfaces.C.Pointers
     (Index         => uint32_t,
      Element       => job_step_info_t,
      Element_Array => job_step_array,
     Default_Terminator => Default_Terminator);
   subtype job_step_info_ptr is job_step_info_ptrs.Pointer;

   type job_step_info_response_msg_ptr is access job_step_info_response_msg;
   package job_step_info_msg_ptr_ptrs is new System.Address_To_Access_Conversions
     (Object => job_step_info_response_msg_ptr);
   subtype job_step_info_msg_ptr_ptr is job_step_info_msg_ptr_ptrs.Object_Pointer;

   function Build_Map (Buffer : aliased access job_step_info_response_msg) return List is
      use job_step_info_ptrs;
      Step_Ptr : job_step_info_ptr;
      S        : Job_Step;
      The_List : List;

   begin
      Step_Ptr := Buffer.job_steps;
      for I in 1 .. Buffer.job_step_count loop
         Init (S, Step_Ptr);
         Insert (The_List, Get_ID (S), S);
         Increment (Step_Ptr);
      end loop;
      slurm_free_job_step_info_response_msg (Buffer);
      return The_List;
   end Build_Map;

   overriding function Element (Position : Cursor) return Job_Step is
   begin
      return Lists.Element (Lists.Cursor (Position));
   end Element;

   function First (Collection : List) return Cursor is
   begin
      return Cursor (Lists.First (Collection.Container));
   end First;

   function Get_CPUs (S : Job_Step) return Natural is
   begin
      return S.CPUs;
   end Get_CPUs;

   function Get_Full_ID (S : Job_Step) return String is
      use Ada.Strings;
      use Ada.Strings.Fixed;
   begin
      if S.Kind = Normal then
         return Trim (S.ID'Img, Left);
      else
         return S.Kind'Img;
      end if;
   end Get_Full_ID;

   function Get_ID (S : Job_Step) return Integer is
   begin
      return S.ID;
   end Get_ID;

   function Get_Kind (S : Job_Step) return Step_Kind is
   begin
      return S.Kind;
   end Get_Kind;

   function Get_Nodes (S : Job_Step) return String is
   begin
      return To_String (S.Nodes);
   end Get_Nodes;

   function Get_Start (S : Job_Step) return Ada.Calendar.Time is
   begin
      return S.Start_Time;
   end Get_Start;

   function Get_Submit_Line (S : Job_Step) return String is
   begin
      return To_String (S.Submit_Line);
   end Get_Submit_Line;

   overriding function Has_Element (Position : Cursor) return Boolean is
   begin
      return Lists.Has_Element (Lists.Cursor (Position));
   end Has_Element;

   procedure Init (S : out Job_Step; Ptr : access job_step_info_t) is
      use Slurm.Low_Level.Utils;
      ID : uint32_t := Ptr.all.step_id.step_id;
   begin
      S.CPUs := Integer (Ptr.all.num_cpus);
      case ID is
         when SLURM_PENDING_STEP =>
            S.Kind := Pending;
            S.ID := -1;
         when SLURM_EXTERN_CONT =>
            S.Kind := Container;
            S.ID := -2;
         when SLURM_BATCH_SCRIPT =>
            S.Kind := Batch;
            S.ID := -3;
         when SLURM_INTERACTIVE_STEP =>
            S.Kind := Interactive;
            S.ID := -4;
         when others =>
            S.Kind := Normal;
            S.ID := Integer (ID);
      end case;
      S.Nodes := Convert_String (Ptr.all.nodes);
      S.Start_Time := Convert_Time (Ptr.all.start_time);
      S.Submit_Line := Convert_String (Ptr.all.submit_line);
   end Init;

   procedure Insert (Collection : in out List;
                     Key        : Integer;
                     Item       : Job_Step) is
   begin
      Lists.Insert (Collection.Container, Key, Item);
   end Insert;

   function Is_Empty (Collection : List) return Boolean is
   begin
      return Lists.Is_Empty (Collection.Container);
   end Is_Empty;

   procedure Iterate
     (Collection : List;
      Process    : not null access procedure (Position : Cursor))
   is
      procedure Wrapper (Position : Lists.Cursor);

      procedure Wrapper (Position : Lists.Cursor) is
      begin
         Process (Cursor (Position));
      end Wrapper;

   begin
      Lists.Iterate (Collection.Container, Wrapper'Access);
   end Iterate;

   function Load_Job_Steps (Job : Natural) return List is
      use Slurm.Errors;
      use Interfaces.C;
      use job_step_info_msg_ptr_ptrs;
      E : Error;
      Buffer : aliased job_step_info_response_msg_ptr;
   begin
      if slurm_get_job_steps (update_time        => 0,
                              job_id             => unsigned (Job),
                              step_id            => NO_VAL,
                              step_response_pptr => To_Address (Buffer'Unchecked_Access),
                              show_flags          => 0) /= 0
      then
         E := Get_Last_Error;
         case E is
            when Protocol_Version_Error =>
               raise Internal_Error with "Incompatible protocol version";
            when Socket_Timeout =>
               raise Internal_Error with "Couldn't contact slurm controller";
            when others =>
               raise Constraint_Error with Get_Error (E);
         end case;
      end if;
      return Build_Map (Buffer);
   end Load_Job_Steps;

   overriding procedure Next (Position : in out Cursor) is
   begin
      Lists. Next (Lists.Cursor (Position));
   end Next;

end Slurm.Job_Steps;
