with Ada.Strings.Fixed;
with Interfaces.C.Strings;
use Interfaces.C;
with Ada.Calendar.Conversions;
with POSIX.C;
with Slurm.Low_Level.bits_stdint_uintn_h;
with Slurm.Low_Level.slurm_slurm_h;
with Slurm.Utils; use Slurm.Utils;

package body Slurm.Low_Level.Utils is

   function Convert_Integer (Source : uint32_t) return Integer is
   begin
      if Source = Slurm.Low_Level.slurm_slurm_h.INFINITE then
         return Integer'Last;
      else
         return Integer (Source);
      end if;
   end Convert_Integer;

   function Convert_Minutes (Source : Interfaces.C.unsigned) return Duration is
   begin
      if Source = Slurm.Low_Level.slurm_slurm_h.INFINITE then
         return Infinite_Duration;
      elsif Source = Slurm.Low_Level.slurm_slurm_h.NO_VAL then
         return Invalid_Duration;
      else
         return Ada.Calendar.Conversions.To_Duration (tv_sec  => long (60 * Source),
                                                      tv_nsec => 0);
      end if;
   end Convert_Minutes;

   function Convert_Seconds (Source : Interfaces.C.unsigned) return Duration is
   begin
      if Source = Slurm.Low_Level.slurm_slurm_h.INFINITE then
         return Infinite_Duration;
      elsif Source = Slurm.Low_Level.slurm_slurm_h.NO_VAL then
         return Invalid_Duration;
      else
         return Ada.Calendar.Conversions.To_Duration (tv_sec  => long (Source),
                                                      tv_nsec => 0);
      end if;
   end Convert_Seconds;

   function Convert_String (Source : chars_ptr) return Unbounded_String is
   begin
      return To_Unbounded_String (To_String (Source));
   end Convert_String;

   function Convert_Time (Source : time_t) return Ada.Calendar.Time is
   begin
      return Ada.Calendar.Conversions.To_Ada_Time
        (Interfaces.C.long (Source));
   end Convert_Time;

   function MiB_To_Gigs (Source : Slurm.Low_Level.bits_stdint_uintn_h.uint32_t) return Gigs is
   begin
      return Gigs (Usage_Number (Source) / 1024);
   end MiB_To_Gigs;

   function MiB_To_Gigs (Source : Slurm.Low_Level.bits_stdint_uintn_h.uint64_t) return Gigs is
   begin
      return Gigs (Usage_Number (Source) / 1024);
   end MiB_To_Gigs;

   function To_String (Source : Interfaces.C.Strings.chars_ptr) return String is
      use Interfaces.C.Strings;
   begin
      declare
         Result : String := Value (Source);
      begin
         return Result;
      end;
   exception
      when Dereference_Error =>
         return "";
   end To_String;

end Slurm.Low_Level.Utils;
