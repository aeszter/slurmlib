with Ada.Strings.Fixed;
with Interfaces.C.Strings; use Interfaces.C.Strings;

package body Slurm.Utils is

   function getpwnam (c_name : chars_ptr) return POSIX.C.passwd_ptr;
   pragma Import (C, getpwnam, "getpwnam");

   function getpwuid (c_uid : POSIX.C.uid_t) return POSIX.C.passwd_ptr;
   pragma Import (C, getpwuid, "getpwuid");

   function Convert_User (UID : Interfaces.C.unsigned) return User_Name is
      use POSIX.C;
      pw_entry : passwd_ptr;
   begin
      pw_entry := getpwuid (uid_t (UID));
      if pw_entry = null
      then
         raise Constraint_Error;
      end if;
      return To_User_Name (POSIX.To_String (Form_POSIX_String (pw_entry.all.pw_name)));
   end Convert_User;

   function To_String (Memory : Gigs) return String is
   begin
      return Ada.Strings.Fixed.Trim (Source => Memory'Img,
                                     Side   => Ada.Strings.Right);
   end To_String;

   function To_String (User : User_Name) return String is
   begin
      return Ada.Strings.Fixed.Trim (String (User), Ada.Strings.Right);
   end To_String;

   function To_String_Set (Source : String) return String_Sets.Set is
      Result : String_Sets.Set := String_Sets.Empty_Set;
      Last   : Integer := Source'First - 1;
      Next   : Integer;
   begin
      while Last < Source'Last loop
         Next := Ada.Strings.Fixed.Index (Source  => Source,
                                          Pattern => ",",
                                          From    => Last + 1);
         if Next = 0 then
            Next := Source'Last + 1;
         end if;
         Result.Include (To_Unbounded_String (Source (Last + 1 ..  Next - 1)));
         Last := Next;
      end loop;
      return Result;
   end To_String_Set;

   function To_UID (Name : String) return POSIX.C.uid_t is
      use POSIX.C;
      use Interfaces.C.Strings;

      pw_entry : passwd_ptr := getpwnam (New_String (Name));
   begin
      if pw_entry = null
      then
         raise Constraint_Error;
      end if;
      return pw_entry.all.pw_uid;
   end To_UID;

   function To_User_Name (User : String) return User_Name is
      use Ada.Strings.Fixed;
   begin
      return User_Name (Head (Source => User,
                              Count  => User_Name'Length));
   end To_User_Name;

end Slurm.Utils;
