with Ada.Containers.Ordered_Maps;
with Ada.Containers.Ordered_Sets;

with Slurm.Node_Properties; use Slurm.Node_Properties;
with Slurm.Nodes;
with Slurm.Gres;
with Slurm.Utils; use Slurm.Utils;
with Slurm.Tres;
with Slurm.Loggers;
with Slurm.Hostlists; use Slurm.Hostlists;

-- @summary This package provides types and subprograms that deal with
-- groups of nodes that share the same properties.
package Slurm.Nodegroups is
   type Nodegroup is new Slurm.Loggers.Logger with private;
   -- an opaque type that represents one group of nodes

   package Countable_Sets is new Ada.Containers.Ordered_Sets (Element_Type => Node_Name);
   -- can be used to keep track of nodes that are in a certain state
   package Countable_Maps is new Ada.Containers.Ordered_Maps (Key_Type     => Node_Name,
                                                              Element_Type => Natural);
   -- can be used to keep track of nodes that have a numerical value
   -- for a certain property (e.g. free CPUs)
   type Countable_Map is new Countable_Maps.Map with null record;
   type Summarized_List is private;
   -- a list of node groups

   overriding procedure Include
     (Container : in out Countable_Map;
      Key       : Node_Name;
      New_Item  : Natural);
   -- add a node and a scalar value belonging to it to a container
   -- @param Container the containner to add to
   -- @param Key the name of the node to add
   -- @param New_Item the value (e.g. free CPUs) pertaininng to the
   -- node

   function Sum (Over : Countable_Map) return Natural;
   -- compute the sum of the values of all nodes in a collection
   -- @param Over the collection to sum over
   -- @return the sum of the values of all nodes in Over

   function Load return Summarized_List;
   -- load all nodes from Slurm, and combine them into node groups
   -- @return the list of all nodes groups

   type State is (total, available, used, reserved, draining, drained, offline);
   -- several states that can be assigned to a node
   -- @value total not really a state, it is assigned to every node,
   -- so that the number of "total" nodes is the total number of
   -- nodes
   -- @value available a node that can take jobs
   -- @value used a node that has at least one job
   -- @value reserved a node that is blocked by a reservation
   -- @value draining a node that has jobs, but will not take new ones
   -- because it has been "drained" by an admin
   -- @value drained a node that does not have jobs and will not take
   -- new ones
   -- @value offline a node that has slurm state down, or is
   -- unreachable
   type State_Count is array (State) of Countable_Map;
   -- this type collects nodes and CPUs in each state

   function To_String (Source : State) return String;
   -- get a string representation of a state
   -- @param Source the state to represent
   -- @return a string representing Source

   procedure Iterate (Collection : Summarized_List;
                      Process    : not null access procedure (G : Nodegroup));
   -- call an arbitrary procedure for each node group in a list
   -- @param Collection the list over which to iterate
   -- @param Process is called once for every nodegroup in Collection
   -- @param G the nodegroup for which Process is called
   procedure Iterate_Summary (Process : not null access procedure (Item : State));
   -- call an arbitrary procedure for each entry in the global summary
   -- of nodes (currently unimplemented)
   -- @param Process the procedure to call for each entry
   -- @param Item Process is called once for each entry in the global
   -- summary; in each call, Item will contain that entry
   function Get_Summary (List : Summarized_List; From : State) return Natural;
   -- compute the sum of CPUs of all nodes in a given state
   -- @param List the list of nodegroups to sum over
   -- @param From the state over which to sum
   -- @return the sum of CPUs of nodes with state From in List

   function New_Nodegroup (Properties : Set_Of_Properties;
                           Meta       : Boolean := False) return Nodegroup;
   -- create a new node group
   -- @param Properties the properties that describe the nodes that
   -- should belong to the new node groupt
   -- @param Meta if true, this is a group of
   -- node groups
   -- @return the new nodegroup
   function "=" (Left : Nodegroup; Right : Slurm.Nodes.Node) return Boolean;
   -- check if a nodegroup containes a node
   -- @param Left the node group to check
   -- @param Right the node to check
   -- @return True iff Left contains Right
   function "=" (Left : Slurm.Nodes.Node; Right : Nodegroup) return Boolean;
   -- check if a node belongs to a node group
   -- @param Left the node to check
   -- @param Right the node group to check
   -- @return True iff Left belongs to Right

   function Get_Available_Nodes (G : Nodegroup) return Natural;
   -- determine the number of available nodes
   -- @param G the node group to check
   -- @return the number of available nodes in G
   function Get_Total_Nodes (G : Nodegroup) return Natural;
   -- determine the number of total nodes
   -- @param G the node group to check
   -- @return the number of total nodes in G
   function Get_Offline_Nodes (G : Nodegroup) return Natural;
   -- determine the number of offline nodes
   -- @param G the node group to check
   -- @return the number of offline nodes in G
   function Get_Used_Nodes (G : Nodegroup) return Natural;
   -- determine the number of used nodes
   -- @param G the node group to check
   -- @return the number of used nodes in G
   function Get_Drained_Nodes (G : Nodegroup) return Natural;
   -- determine the number of drained nodes
   -- @param G the node group to check
   -- @return the number of drained nodes in G
   function Get_Draining_Nodes (G : Nodegroup) return Natural;
   -- determine the number of draining nodes
   -- @param G the node group to check
   -- @return the number of draining nodes in G

   function Get_Available_Cores (G : Nodegroup) return Natural;
   -- determine the number of available cores
   -- @param G the node group to check
   -- @return the number of available cores in G
   function Get_Total_Cores (G : Nodegroup) return Natural;
   -- determine the number of total cores
   -- @param G the node group to check
   -- @return the number of total cores in G
   function Get_Offline_Cores (G : Nodegroup) return Natural;
   -- determine the number of offline cores
   -- @param G the node group to check
   -- @return the number of offline cores in G
   function Get_Used_Cores (G : Nodegroup) return Natural;
   -- determine the number of used cores
   -- @param G the node group to check
   -- @return the number of used cores in G
   function Get_Drained_Cores (G : Nodegroup) return Natural;
   -- determine the number of drained cores
   -- @param G the node group to check
   -- @return the number of drained cores in G

   function Get_Memory (G : Nodegroup) return Gigs;
   -- determine the amount of memory per node
   -- @param G the node group to check
   -- @return the amount of memory each node in G has
   function Get_CPUs (G : Nodegroup) return Natural;
   -- determine the number of cpus per node
   -- @param G the node group to check
   -- @return the number of cpus each node in G has
   function Get_GRES (G : Nodegroup) return Slurm.Gres.List;
   -- determine the GRES per node
   -- @param G the node group to check
   -- @return a list of GRES each node in G has
   function Get_TRES (G : Nodegroup) return Slurm.Tres.List;
   -- determine the TRES per node
   -- @param G the node group to check
   -- @return a list of TRES each node in G has
   function Get_Features (G : Nodegroup) return String;
   -- determine the features per node
   -- @param G the node group to check
   -- @return the features each node in G has
   function Has_IB (G : Nodegroup) return Boolean;
   -- determine whether nodes have infiniband
   -- @param G the node group to check
   -- @return whether nodes in G have infiniband
   function Is_Meta_Group (G : Nodegroup) return Boolean;
   -- determine whether G is a meta group, i.e. it is a group of node
   -- groups
   -- @param G the node group to check
   -- @return whether G is a meta group

private
   type Nodegroup is new Slurm.Loggers.Logger with record
      Available_Nodes, Total_Nodes,
      Drained_Nodes, Draining_Nodes,
      Offline_Nodes, Used_Nodes : Countable_Sets.Set;
      Available_CPUs,
      Draining_CPUs,
      Offline_CPUs, Used_CPUs   : Countable_Map;
      Meta : Boolean := False;

      Properties : Set_Of_Properties;
   end record;

   package Lists is new Ada.Containers.Ordered_Maps (Element_Type => Nodegroup,
                                                    Key_Type => Set_Of_Properties);

   type Summarized_List is new Lists.Map with
   record
     Summary : State_Count;
   end record;

   overriding function Copy (Source : Summarized_List) return Summarized_List;

end Slurm.Nodegroups;
