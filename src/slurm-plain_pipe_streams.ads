with POSIX.IO; use POSIX.IO;
with POSIX.Process_Identification;
with POSIX; use POSIX;
with Slurm.Taint; use Slurm.Taint;

--  @summary Stream read from a pipe, inspired by xmlADA,
--  but for plain latin_1 characters without any prolog
package Slurm.Plain_Pipe_Streams is

   type Plain_Pipe_Stream is tagged private;
   -- an opaque type representing the pipe to one program

   Failed_Creation_Error : exception;
   -- couldn't launch the external program
   Exception_Error : exception;
   -- the external program terminated because of an exception
   Other_Error : exception;
   -- any other fatal error
   procedure Next_Char (From : in out Plain_Pipe_Stream;
      C    : out Character);
   --  Return a single character from From.
   --  @param From the pipe to read from
   --  @param C the character read
   function Eof (From : Plain_Pipe_Stream) return Boolean;
   --  Return True if there is no more character to read on the stream
   --  @param From the pipe to check
   --  @return True iff the stream has reached end of file
   procedure Close (Input : in out Plain_Pipe_Stream; Exit_Status : out Natural);
   -- Close the pipe
   -- @param Input the stream to close
   -- @param Exit_Status the POSIX exit status of the external program
   -- @exception Failed_Creation_Error if the external program
   -- couldn't be launched;
   -- Exception_Error if the external program terminated
   -- because of an unhandled exception

   procedure Execute (P : in out Plain_Pipe_Stream;
                   Command : Trusted_Command_Name;
                   Arguments : Trusted_String_List);
   -- Launch an external program and connect its output to a pipe
   -- stream
   -- @param P the pipe stream to connect
   -- @param Command the name of the command to launch
   -- @param Arguments any arguments to pass to the program
private
   type Plain_Pipe_Stream is tagged record
      Pipe        : File_Descriptor;
      Buffer      : IO_Buffer (1 .. 1_024);
      Position    : Natural := 0;
      Last_Read   : IO_Count := 0;
      --  points after the character last read
      Eof_Reached : Boolean := False;
      PID         : Process_Identification.Process_ID;
   end record;
end Slurm.Plain_Pipe_Streams;
