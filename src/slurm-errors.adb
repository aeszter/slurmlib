with Interfaces.C; use Interfaces.C;

with Slurm.Low_Level.slurm_slurm_errno_h; use Slurm.Low_Level.slurm_slurm_errno_h;
with Interfaces.C.Strings;

package body Slurm.Errors is

   function Get_Error (errno : Error) return String is
   begin
      return Interfaces.C.Strings.Value (slurm_strerror (Interfaces.C.int (errno)));
   end Get_Error;

   function Get_Last_Error return Error is
      errno : Interfaces.C.int := slurm_get_errno;
   begin
      return Error (errno);
   end Get_Last_Error;

end Slurm.Errors;
