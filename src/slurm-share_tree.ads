with Slurm.Utils;
use Slurm.Utils;
with Ada.Containers.Doubly_Linked_Lists;

-- @summary This package loads user priority data following the Share
-- Tree paradigm from slurm and makes it available to the application
package Slurm.Share_Tree is

   subtype User_Name_String is Slurm.Utils.User_Name;

   type User_Node is record
      User_Name       : User_Name_String;
      Raw_Shares      : Usage_Number;
      Norm_Shares     : Usage_Number;
      Raw_Usage       : Usage_Integer;
      Effective_Usage : Usage_Number;
      Fairshare       : Usage_Number;
   end record;
   -- one node in the tree, representing one user
   -- @field User_Name the name of the user this record represents
   -- @field Raw_Shares the share of the cluster the user should use
   -- in the long term
   -- @field Norm_Shares the share of the cluser the user should use,
   -- normalized
   -- @field Raw_Usage how many TRES-seconds the user has actually
   -- used
   -- @field Effective_Usage how much of the cluster the user has
   -- actually used, normalized to one tier in the share tree
   -- @field Fairshare the priority the user gets; the higher the
   -- user's shares and the lower their actual usage, the higher this
   -- will be

   package Lists is new Ada.Containers.Doubly_Linked_Lists (Element_Type => User_Node);
   -- a collection of User_Node records

   Format_Error : exception; -- unexpected data from sshare

   procedure Load;
   -- load priority data for all users
   -- @exception Format_Error if sshare produces unexpected output
   procedure Iterate (Process : not null access procedure (Position : Lists.Cursor));
   -- for all User_Node records loaded, call an arbitrary procedure
   -- @param Process the procedure to call
   -- @param Position a cursor pointing to the User_Node currently
   -- being processed

   procedure Sort_By_User;
   -- sort the internal list of User_Nodes by user name
   procedure Sort_By_Usage;
   -- sort the internal list of User_Nodes by actual usage
   procedure Sort_By_Fairshare;
   -- sort the internal list of User_Nodes by priority
   procedure Sort_By_Shares;
   -- sort the internal list of User_Nodes by shares (i.e., desired
   -- usage)
   procedure Reverse_Order;
   -- reverse the order of the internal list

private
   function Precedes_By_User (Left, Right : User_Node) return Boolean;
   function Precedes_By_Usage (Left, Right : User_Node) return Boolean;
   function Precedes_By_Fairshare (Left, Right : User_Node) return Boolean;
   function Precedes_By_Shares (Left, Right : User_Node) return Boolean;

   package Sorting_By_User is new Lists.Generic_Sorting ("<" => Precedes_By_User);
   package Sorting_By_Usage is new Lists.Generic_Sorting ("<" => Precedes_By_Usage);
   package Sorting_By_Fairshare is new Lists.Generic_Sorting ("<" => Precedes_By_Fairshare);
   package Sorting_By_Shares is new Lists.Generic_Sorting ("<" => Precedes_By_Shares);

end Slurm.Share_Tree;
