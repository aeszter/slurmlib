with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C; use Interfaces.C;

with POSIX.C; use POSIX.C;
with Slurm.Errors;
with Slurm.Low_Level.slurm_slurm_h; use Slurm.Low_Level.slurm_slurm_h;
with Interfaces; use Interfaces;
with Slurm.Low_Level.slurm_slurm_h;
with Slurm.Low_Level.bits_stdint_uintn_h;
use Slurm.Low_Level.bits_stdint_uintn_h;

package body Slurm.Admin is

   NODE_STATE_RESUME : constant Unsigned_64 := Shift_Left (1, 8);
   NODE_STATE_DRAIN  : constant Unsigned_64 := Shift_Left (1, 9);
   NODE_STATE_UNDRAIN : constant Unsigned_64 := Shift_Left (1, 6);
   procedure Down_Node (Name, Reason : String; uid : uid_t) is
      use Slurm.Errors;

      node_msg : aliased update_node_msg_t;
      Result   : Interfaces.C.int;
      API_Err  : Error;
   begin
      slurm_init_update_node_msg (node_msg'Unchecked_Access);
      node_msg.node_names := New_String (Name);
      node_msg.reason := New_String (Reason);
      node_msg.node_state := node_states'Enum_Rep (NODE_STATE_DOWN);
      node_msg.reason_uid := uint32_t (uid);
      Result := slurm_update_node (node_msg'Unchecked_Access);
      if Result /= 0 then
         API_Err := Get_Last_Error;
         if Get_Last_Error = User_ID_Missing then
            raise Slurm_Error with "Not allowed to down node" & Name;
         else
            raise Slurm_Error with Get_Error (API_Err);
         end if;
      end if;
   end Down_Node;

   procedure Drain_Node (Name, Reason : String; UID : uid_t) is
      use Slurm.Errors;

      node_msg : aliased update_node_msg_t;
      Result   : Interfaces.C.int;
      API_Err  : Error;
   begin
      slurm_init_update_node_msg (node_msg'Unchecked_Access);
      node_msg.node_names := New_String (Name);
      node_msg.reason := New_String (Reason);
      node_msg.reason_uid := uint32_t (UID);
      node_msg.node_state := uint32_t (NODE_STATE_DRAIN);
      Result := slurm_update_node (node_msg'Unchecked_Access);
      if Result /= 0 then
         API_Err := Get_Last_Error;
         if Get_Last_Error = User_ID_Missing then
            raise Slurm_Error with "Not allowed to drain node" & Name;
         else
            raise Slurm_Error with Get_Error (API_Err);
         end if;
      end if;
   end Drain_Node;

   procedure Kill_Job (ID : Positive) is
      use Slurm.Errors;
      Result : Interfaces.C.int;
      Reason : Error;
   begin
      Result := slurm_kill_job (job_id => uint32_t (ID),
                               signal => 9,
                               flags  => 0);
      if Result /= 0 then
         Reason := Get_Last_Error;
         if Get_Last_Error = User_ID_Missing then
            raise Slurm_Error with "Not allowed to kill job" & ID'Img;
         else
            raise Slurm_Error with Get_Error (Reason);
         end if;
      end if;
   end Kill_Job;

   procedure Release_Job (ID : Positive) is
      use Slurm.Errors;

      job_msg : aliased job_desc_msg_t;
      Result  : Interfaces.C.int;
      Reason  : Error;
   begin
      slurm_init_job_desc_msg (job_msg'Unchecked_Access);
      job_msg.job_id := uint32_t (ID);
      job_msg.priority := uint32_t'Last;
      Result := slurm_update_job (job_msg'Unchecked_Access);
      if Result /= 0 then
         Reason := Get_Last_Error;
         if Get_Last_Error = User_ID_Missing then
            raise Slurm_Error with "Not allowed to release job" & ID'Img;
         else
            raise Slurm_Error with Get_Error (Reason);
         end if;
      end if;
   end Release_Job;

   procedure Resume_Node (Name : String) is
      use Slurm.Errors;

      node_msg : aliased update_node_msg_t;
      Result  : Interfaces.C.int;
      Reason  : Error;
   begin
      slurm_init_update_node_msg (node_msg'Unchecked_Access);
      node_msg.node_names := New_String (Name);
      node_msg.node_state := uint32_t (NODE_STATE_RESUME);
      Result := slurm_update_node (node_msg'Unchecked_Access);
      if Result /= 0 then
         Reason := Get_Last_Error;
         if Get_Last_Error = User_ID_Missing then
            raise Slurm_Error with "Not allowed to resume node" & Name;
         else
            raise Slurm_Error with Get_Error (Reason);
         end if;
      end if;
   end Resume_Node;

   procedure Undrain_Node (Name : String) is
      use Slurm.Errors;

      node_msg : aliased update_node_msg_t;
      Result  : Interfaces.C.int;
      Reason  : Error;
   begin
      slurm_init_update_node_msg (node_msg'Unchecked_Access);
      node_msg.node_names := New_String (Name);
      node_msg.node_state := uint32_t (NODE_STATE_UNDRAIN);
      Result := slurm_update_node (node_msg'Unchecked_Access);
      if Result /= 0 then
         Reason := Get_Last_Error;
         if Get_Last_Error = User_ID_Missing then
            raise Slurm_Error with "Not allowed to undrain node" & Name;
         else
            raise Slurm_Error with Get_Error (Reason);
         end if;
      end if;
   end Undrain_Node;

end Slurm.Admin;
