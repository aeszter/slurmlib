#!/bin/bash
rm -rf bindings
mkdir -p bindings
( cd bindings; g++ -c -fdump-ada-spec -C ../lib/include/slurm/slurm.h -fada-spec-parent=Slurm.Low_Level )
sed -f c.sed -i bindings/*.ads 
if grep -q 'typedef struct hostlist [*] hostlist_t' lib/include/slurm/slurm.h; then
  sed -f hostlist-old.sed -i bindings/*.ads
else
  sed -f hostlist-new.sed -i bindings/*.ads
fi
